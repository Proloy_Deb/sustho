package com.susthoxyz.sustho.Appointment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Model.OnlineDoctor;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;

public class AppointmentActivity extends AppCompatActivity {

    private ArrayList<OnlineDoctor> doctors;
    private OnlineDoctorAdapter onlineDoctorAdapter ;
    private RecyclerView.Adapter adapter;
    private RecyclerView rvDocotorList ;
    private SharedPreferences sharedPreferences;
//    ArrayList<String> departments;
//    private String url = "http://api.sustho.xyz/sustho/get_doctors.php";
//    private String key = "give_all_doctors";

//    private String mobile = null;
//    private String token = null;
    private String department ;
    private DatabaseReference reference ;
    private Toolbar toolbar ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        doctors = new ArrayList<>();
        adapter = new OnlineDoctorAdapter(doctors,AppointmentActivity.this);
        toolbar = findViewById(R.id.toolBarAppointment);
        rvDocotorList = findViewById(R.id.rvDoctors);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
//        id = sharedPreferences.getString("user_id",null);
//        token = sharedPreferences.getString("token",null);
//        mobile = sharedPreferences.getString("mobile",null);
        reference = FirebaseDatabase.getInstance().getReference().child("Doctors");

        rvDocotorList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

       /* spDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                department =  spDepartment.getItemAtPosition(spDepartment.getSelectedItemPosition()).toString();
                Toast.makeText(getApplicationContext(),department,Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });*/
        /*linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(rvDocotorList.getContext(), linearLayoutManager.getOrientation());

        rvDocotorList.setHasFixedSize(true);
        rvDocotorList.setLayoutManager(linearLayoutManager);
        rvDocotorList.addItemDecoration(dividerItemDecoration);
        rvDocotorList.setAdapter(adapter);*/

        Doctors();
//        department();
    }

   /* private void DoctorList() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("doctors");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String doctorName = dataobj.getString("doctor_name");
                                String doctorDegree = dataobj.getString("doctor_degree");
                                String doctorDay = dataobj.getString("doctor_day");
                                String doctorTime = dataobj.getString("doctor_appointment_time");
                                String doctorHospitalAddress = dataobj.getString("doctor_hospital_address");
                                String departmentName = dataobj.getString("doctor_department_name");
                                String doctorImage = dataobj.getString("doctor_image");
                                String doctorHospital = dataobj.getString("doctor_hospital_name");
                                String mobile = dataobj.getString("doctor_phnNo");
                                String email = dataobj.getString("doctor_email");
                                String doctorId = dataobj.getString("doctor_id");
                                String doctorFee = dataobj.getString("doctor_fees");

                                OnlineDoctor doctor = new OnlineDoctor(doctorName,doctorDegree,doctorImage,doctorHospitalAddress,doctorTime,departmentName,
                                        doctorHospital,mobile,email,doctorId,doctorFee);
                                doctors.add(doctor);

                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("phn_no",mobile);
                params.put("user_id",id);
                params.put("doctors",key);

                return params;
            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }*/

    private void Doctors()
    {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                doctors = new ArrayList<OnlineDoctor>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    OnlineDoctor doctor = dataSnapshot1.getValue(OnlineDoctor.class);
                    doctors.add(doctor);

                }
                onlineDoctorAdapter = new OnlineDoctorAdapter( doctors,AppointmentActivity.this);
                rvDocotorList.setAdapter(onlineDoctorAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

   /* private void department()
    {
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference().child("Departments");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                departments = new ArrayList<String>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

//                    Departments departmentss = dataSnapshot1.getValue(Departments.class);
                    String departmentss = dataSnapshot1.child("departmenName").getValue().toString();
                    departments.add(departmentss);

                }
//                onlineDoctorAdapter = new OnlineDoctorAdapter( doctors,AppointmentActivity.this);
                spDepartment.setAdapter(new ArrayAdapter<String>(AppointmentActivity.this, android.R.layout.simple_spinner_dropdown_item, departments));
//                rvDocotorList.setAdapter(onlineDoctorAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }*/

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
