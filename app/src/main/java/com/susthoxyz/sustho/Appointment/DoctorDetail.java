package com.susthoxyz.sustho.Appointment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.ConfirmDialog;
import com.susthoxyz.sustho.R;

public class DoctorDetail extends AppCompatActivity  implements ConfirmDialog.ConfirmDialogListener {

    private TextView tvName, tvHopsital,tvAddress,tvEmail,tvStartTime,tvEndTime,tvTimeType,tvSpecialist,tvDegree ;
    private String  doctorname,doctorhospital,hospitaladdress, doctordetail,doctorStarttime,doctorEndTime, doctorDepartment,
            image,doctorDegree,doctorFee,timetype;
    private CircularImageView imvDoctorProfile ;
    private Button btnDoctorApointment ;
    private String date ;
    SharedPreferences sharedPreferences ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_detail);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);

        btnDoctorApointment = findViewById(R.id.btnAppointmetn);
        imvDoctorProfile = findViewById(R.id.imvDoctorDetail);
        tvName = findViewById(R.id.tvDocDetailName);
        tvAddress = findViewById(R.id.tvDetailLocation);
        tvEmail = findViewById(R.id.tvDetailEmail);
        tvSpecialist = findViewById(R.id.tvDocDetailSpecialist);
        tvDegree = findViewById(R.id.tvDocDetailDegree);
        tvHopsital = findViewById(R.id.tvDetailHospitalName);
        tvStartTime = findViewById(R.id.tvStartTime);
        tvEndTime = findViewById(R.id.tvEndTime);
        tvTimeType = findViewById(R.id.tvTimeType);

        doctorname = sharedPreferences.getString("doctorname",null);
        doctorhospital = sharedPreferences.getString("doctorhospital",null);
        hospitaladdress = sharedPreferences.getString("hospitaladdress",null);
        doctordetail = sharedPreferences.getString("doctordetail",null);
        doctorStarttime = sharedPreferences.getString("doctorStarttime",null);
        doctorEndTime = sharedPreferences.getString("doctorEndtime",null);
        timetype = sharedPreferences.getString("timetype",null);
        doctorDepartment = sharedPreferences.getString("doctorDepartment",null);
        image = sharedPreferences.getString("doctorimage",null);
        doctorDegree = sharedPreferences.getString("doctorDegree",null);
        doctorFee = sharedPreferences.getString("doctorFee",null);

        tvName.setText(doctorname);
        tvAddress.setText(hospitaladdress);
        tvEmail.setText(doctordetail);
        tvStartTime.setText(doctorStarttime);
        tvEndTime.setText(doctorEndTime);
        tvSpecialist.setText(doctorFee);
        tvDegree.setText(doctorDegree);
        tvHopsital.setText(doctorhospital);
        tvTimeType.setText(timetype);

        Picasso.get().load(image).into(imvDoctorProfile);

        btnDoctorApointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppointmentTime appointmentTime = new AppointmentTime();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameDoctor,appointmentTime).addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


    }

    @Override
    public void applyTexts(String username) {

    }
}
