package com.susthoxyz.sustho.Appointment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.susthoxyz.sustho.Activity.RegistrationActivity;
import com.susthoxyz.sustho.ConfirmDialog;
import com.susthoxyz.sustho.Fragment.BkashFragment;
import com.susthoxyz.sustho.Fragment.DatePickerFragment;
import com.susthoxyz.sustho.Model.Time;
import com.susthoxyz.sustho.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AppointmentTime extends Fragment implements ConfirmDialog.ConfirmDialogListener {

    SharedPreferences sharedPreferences ;
    private String doctorID , doctorName , doctorHospital,doctorStartTime ,doctorEndTime,doctorDepartment,currentDateFormat,
            appointmentTime,mobile;

    private RelativeLayout rlCalender ;
    private GridLayoutManager gridLayoutManager;
    private List<Time> times;
    private RecyclerView.Adapter adapter;
    private RecyclerView rvAppoitnTime ;
    private TextView tvDoctorName , tvHospital , tvDepartment, tvDate ;
    private Button btnApointmentTime ;
    private DatabaseReference reference ;
    private String token = null;
/*
    private String url = "http://api.sustho.xyz/sustho/get_appointment.php";
    private String apointmentUrl = "http://api.sustho.xyz/sustho/user_book_appointment.php";
    private String key = "give_appointment_time";
    private String apointmentConfirmKey = "book_appointment";*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_appointment_time, container, false);

        tvDoctorName = rootView.findViewById(R.id.tvHeaderDocName);
        tvHospital = rootView.findViewById(R.id.tvAppointmentHospital);
        btnApointmentTime = rootView.findViewById(R.id.btnApointmentConfirm);
        tvDepartment = rootView.findViewById(R.id.tvAppointmentDept);
        rlCalender = rootView.findViewById(R.id.rlCalender);
        tvDate = rootView.findViewById(R.id.tvShowDate);

        sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
//        userId = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);
        mobile = sharedPreferences.getString("mobile",null);

        doctorID = sharedPreferences.getString("doctorId",null);
        doctorName = sharedPreferences.getString("doctorname",null);
        doctorHospital = sharedPreferences.getString("doctorhospital",null);
        doctorStartTime = sharedPreferences.getString("doctorStarttime",null);
        doctorEndTime = sharedPreferences.getString("doctorEndtime",null);
        doctorDepartment = sharedPreferences.getString("doctorFee",null);
        appointmentTime = sharedPreferences.getString("timeslot",null);

        reference = FirebaseDatabase.getInstance().getReference().child("Appointments");
        tvDoctorName.setText(doctorName);
        tvHospital.setText(doctorHospital);
        tvDepartment.setText(doctorDepartment);

        rvAppoitnTime = rootView.findViewById(R.id.rvAppointmentTime);
        gridLayoutManager = new GridLayoutManager(getActivity(),5);
        rvAppoitnTime.setLayoutManager(gridLayoutManager);


        btnApointmentTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(token!= null)
                {
                    appointment();
                }

                else {
                    Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                    startActivity(intent);
                }

//                openDialog();
            }
        });

        rlCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getActivity().getSupportFragmentManager(),"date picker");*/
                showDatePicker();

            }
        });

        try {
            appointmentTime(doctorStartTime,doctorEndTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return rootView;
    }

    private void appointmentTime(String doctorStartTime, String doctorEndTime) throws ParseException {

        String format = "hh:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date dateObj1 = sdf.parse(doctorStartTime);
        Date dateObj2 = sdf.parse(doctorEndTime);

        times = new ArrayList<>();
        long dif = dateObj1.getTime();
        while (dif < dateObj2.getTime()) {
            Date slot = new Date(dif);
            String date = sdf.format(slot);
            dif += 900000;
            Time time = new Time(date,"PM");
            times.add(time);

//            Toast.makeText(getActivity(), date, Toast.LENGTH_SHORT).show();
        }
        adapter = new TimeAdapter(times,getActivity());
        rvAppoitnTime.setAdapter(adapter);

    }

    public void openDialog() {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.show(getActivity().getSupportFragmentManager(), "example dialog");
    }

    private void showDatePicker() {

        DatePickerFragment date = new DatePickerFragment();

        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);

        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");

    }


    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            currentDateFormat = (String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear+1)
                    + "-" + String.valueOf(year));

            tvDate.setText(currentDateFormat);
            sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("date", currentDateFormat);
            editor.commit();
        }
    };


    @Override
    public void applyTexts(String confirm) {

        if(confirm.equals("ok"))
        {
            appointment();
        }
    }

    private void appointment() {

        if (currentDateFormat.isEmpty())
        {
            btnApointmentTime.setError(" তারিখ সিলেক্ট করুন ");
        }

        else {
            String parentKey = reference.push().getKey();
            Appointment appointment = new Appointment(currentDateFormat,appointmentTime,doctorID,mobile,parentKey,doctorHospital,doctorName);

            reference.child(mobile).child(parentKey).setValue(appointment).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(getActivity(), "Appointment Successfully", Toast.LENGTH_SHORT).show();

                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frameDoctor,new BkashFragment()).addToBackStack(null);
                    fragmentTransaction.commit();

                }
            });
        }

    }

     /*  private void appointmentTime() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("doctor_appointment_time");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String timeSlot = dataobj.getString("doctor_appointment_time_slot_sector");
                                String time = dataobj.getString("doctor_appointment_time_slot");

                                Time doctorTime = new Time(time,timeSlot);
                                times.add(doctorTime);

                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("user_id", userId);
                params.put("token",token);
                params.put("doctor_id",doctorID);
                params.put("appointment_time",key);

                return params;
            }
        };

        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }*/

    /*private void appointment() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, apointmentUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");

                            if (status.equals("1"))
                            {
                                Toast.makeText(getContext(), "Book Appointment Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            }

                            else {
                                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("user_id", userId);
                params.put("token",token);
                params.put("doctor_id",doctorID);
                params.put("time",doctorTime);
                params.put("date",currentDateFormat);
                params.put("appointment",apointmentConfirmKey);

                return params;
            }
        };

        Volley.newRequestQueue(getActivity()).add(stringRequest);

    }*/
}
