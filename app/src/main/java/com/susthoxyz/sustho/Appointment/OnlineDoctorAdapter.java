package com.susthoxyz.sustho.Appointment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.Model.OnlineDoctor;
import com.susthoxyz.sustho.R;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class OnlineDoctorAdapter extends RecyclerView.Adapter<OnlineDoctorAdapter.MyViewHolder> {

    private Context mContext;
    private List<OnlineDoctor> mData;
    SharedPreferences sharedPreferences ;
    String doctorname,doctorhospital,hospitaladdress, doctorDetail,doctorStarttime,doctorEndtime,
            doctorDepartment,image,doctorDegree,doctorId,doctorFee,timetype;

    public OnlineDoctorAdapter(List<OnlineDoctor> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_online_doctors, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OnlineDoctorAdapter.MyViewHolder holder, final int position) {
        holder.tvDoctorName.setText(mData.get(position).getDoctorName());
        holder.tvHospital.setText(mData.get(position).getDepartment());
        holder.tvSpecialist.setText(mData.get(position).getDegree());
        Picasso.get().load(mData.get(position).getDoctorImage()).into(holder.imvDoctorProfile);

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorname = mData.get(position).getDoctorName();
                doctorhospital = mData.get(position).getHospital();
                doctorDetail =mData.get(position).getDetail();
                doctorStarttime = mData.get(position).getStartTime();
                doctorEndtime = mData.get(position).getEndTime();
                hospitaladdress = mData.get(position).getAddress();
                doctorDepartment = mData.get(position).getDepartment();
                image = mData.get(position).getDoctorImage();
                doctorDegree = mData.get(position).getDegree();
                doctorId = mData.get(position).getDoctorId();
                doctorFee = mData.get(position).getFee();
                timetype = mData.get(position).getTimeType();

                sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString("doctorname", doctorname);
                editor.putString("doctorhospital", doctorhospital);
                editor.putString("hospitaladdress", hospitaladdress);
                editor.putString("doctordetail", doctorDetail);
                editor.putString("doctorStarttime", doctorStarttime);
                editor.putString("doctorEndtime", doctorEndtime);
                editor.putString("doctorDepartment", doctorDepartment);
                editor.putString("doctorimage", image);
                editor.putString("doctorId",doctorId);
                editor.putString("doctorDegree", doctorDegree);
                editor.putString("doctorFee", doctorFee);
                editor.putString("timetype", timetype);
                editor.commit();

                Intent myactivity = new Intent(mContext.getApplicationContext(), DoctorDetail.class);
                myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.getApplicationContext().startActivity(myactivity);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDoctorName , tvSpecialist , tvHospital ;
        CircularImageView imvDoctorProfile ;
        RelativeLayout parent ;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvSpecialist = itemView.findViewById(R.id.tvDoctorSpecialist);
            tvHospital = itemView.findViewById(R.id.tvHospitalName);
            imvDoctorProfile = itemView.findViewById(R.id.imvDoctorProfile);
            parent = itemView.findViewById(R.id.parentLayout);

        }
    }
}
