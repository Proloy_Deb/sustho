package com.susthoxyz.sustho.Appointment;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.susthoxyz.sustho.Model.Time;
import com.susthoxyz.sustho.R;

import java.util.List;

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.MyViewHolder> {

    private Context mContext;
    private List<Time> mData;
    SharedPreferences sharedPreferences ;

    public TimeAdapter(List<Time> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_time, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TimeAdapter.MyViewHolder holder, final int position) {
        holder.tvTimeSector.setText(mData.get(position).getSector());
        holder.tvTimeClock.setText(mData.get(position).getTime());
        holder.cvTimeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.cvTimeSelect.setCardBackgroundColor(mContext.getResources().getColor(R.color.headertext));
                holder.tvTimeClock.setTextColor(mContext.getResources().getColor(R.color.white));
                holder.tvTimeSector.setTextColor(mContext.getResources().getColor(R.color.white));

                String time = mData.get(position).getTime();
                sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("timeslot", time);
                editor.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTimeSector ,tvTimeClock;
        CardView cvTimeSelect ;
        RelativeLayout parentAdminLayout ;
        Button btnSubmit ;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvTimeSector = itemView.findViewById(R.id.tvTimeSector);
            tvTimeClock = itemView.findViewById(R.id.tvClockTime);
            cvTimeSelect = itemView.findViewById(R.id.cvTimeSelect);

        }
    }
}