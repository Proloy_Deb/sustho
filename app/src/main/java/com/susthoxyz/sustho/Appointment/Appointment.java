package com.susthoxyz.sustho.Appointment;

public class Appointment {

    private String date,time,doctorId,userMobile,appointmentId,doctorHospital,doctorName ;

    public Appointment() {
    }

    public Appointment(String date, String time, String doctorId, String userMobile, String appointmentId,String doctorHospital,String doctorName) {
        this.date = date;
        this.time = time;
        this.doctorId = doctorId;
        this.userMobile = userMobile;
        this.appointmentId = appointmentId;
        this.doctorHospital = doctorHospital ;
        this.doctorName = doctorName ;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorHospital() {
        return doctorHospital;
    }

    public void setDoctorHospital(String doctorHospital) {
        this.doctorHospital = doctorHospital;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }
}
