package com.susthoxyz.sustho.Appointment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.susthoxyz.sustho.R;

import java.util.List;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.MyViewHolder> {

    private Context mContext;
    private List<Appointment> mData;

    public AppointmentAdapter(List<Appointment> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_appointment, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AppointmentAdapter.MyViewHolder holder, final int position) {
        holder.tvDoctorName.setText(mData.get(position).getDoctorName());
        holder.tvHospital.setText(mData.get(position).getDoctorHospital());
        holder.tvTime.setText(mData.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDoctorName,tvHospital,tvTime;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvDoctorName = itemView.findViewById(R.id.tvAppointmentDoctorName);
            tvHospital = itemView.findViewById(R.id.tvAppointmentDoctorHospital);
            tvTime = itemView.findViewById(R.id.tvAppointmentTime);

        }
    }
}