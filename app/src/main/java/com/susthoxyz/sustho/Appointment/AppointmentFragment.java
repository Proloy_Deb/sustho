package com.susthoxyz.sustho.Appointment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;
import java.util.List;


public class AppointmentFragment extends Fragment {

    private String url = "http://api.sustho.xyz/sustho/get_user_book_appointment.php";
    private String token = null;
    private String id = null;
    private String mobile = null ;
    private String key = "give_book_appointment" ;

    private LinearLayoutManager linearLayoutManager;
    private List<Appointment> appointments;
    private RecyclerView rvappointments ;
    private AppointmentAdapter appointmentAdapter ;
    private SharedPreferences sharedPreferences ;
    private DatabaseReference reference ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_appointment, container, false);

        reference = FirebaseDatabase.getInstance().getReference().child("Appointments");
        sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);
        mobile = sharedPreferences.getString("mobile",null);

        rvappointments = rootView.findViewById(R.id.rvAppointMent);
        rvappointments.setLayoutManager(new LinearLayoutManager(getActivity()));

       /* appointments = new ArrayList<>();
        appointmentAdapter = new AppointmentAdapter(appointments,getActivity());

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvappointments.setHasFixedSize(true);
        rvappointments.setLayoutManager(linearLayoutManager);
        rvappointments.setAdapter(appointmentAdapter);*/

        if (id!=null)
        {
            get_appointments();
        }

        return rootView;
    }

    private void get_appointments() {

        reference.child(mobile).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                appointments = new ArrayList<Appointment>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Appointment appointment = dataSnapshot1.getValue(Appointment.class);
                    appointments.add(appointment);

                }
                appointmentAdapter = new AppointmentAdapter( appointments,getActivity());
                rvappointments.setAdapter(appointmentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*private void get_appointments() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("get_book_appointment");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String doctorName = dataobj.getString("doctor_name");
                                String department = dataobj.getString("doctor_department_name");
                                String hospital = dataobj.getString("doctor_hospital_name");
                                String time = dataobj.getString("book_appointment_time");

                                Appointment appointment = new Appointment(doctorName,hospital,department,time);
                                appointments.add(appointment);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), " কোন অর্ডার নেই", Toast.LENGTH_SHORT).show();
                        }

                        appointmentAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("user_id",id);
                params.put("book_appointment",key);

                return params;
            }
        };

        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }*/
}
