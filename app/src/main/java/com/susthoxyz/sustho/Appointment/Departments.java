package com.susthoxyz.sustho.Appointment;

public class Departments {

    private String departmenName;

    public Departments() {
    }

    public Departments(String departmenName) {
        this.departmenName = departmenName;
    }

    public String getDepartmenName() {
        return departmenName;
    }

    public void setDepartmenName(String departmenName) {
        this.departmenName = departmenName;
    }
}
