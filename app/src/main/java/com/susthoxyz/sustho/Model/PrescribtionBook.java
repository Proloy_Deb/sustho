package com.susthoxyz.sustho.Model;

public class PrescribtionBook {

    private String date , image , problem , uploadId;

    public PrescribtionBook() {
    }

    public PrescribtionBook(String date, String image, String problem, String uploadId) {
        this.date = date;
        this.uploadId = uploadId;
        this.image = image;
        this.problem = problem;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }
}
