package com.susthoxyz.sustho.Model;

public class Blog {

    private String blogimage, blogtitle, blogdetail;

    public Blog() {
    }

    public Blog(String blogimage, String blogtitle, String blogdetail) {
        this.blogimage = blogimage;
        this.blogtitle = blogtitle;
        this.blogdetail = blogdetail;
    }

    public String getBlogimage() {
        return blogimage;
    }

    public void setBlogimage(String blogimage) {
        this.blogimage = blogimage;
    }

    public String getBlogtitle() {
        return blogtitle;
    }

    public void setBlogtitle(String blogtitle) {
        this.blogtitle = blogtitle;
    }

    public String getBlogdetail() {
        return blogdetail;
    }

    public void setBlogdetail(String blogdetail) {
        this.blogdetail = blogdetail;
    }
}
