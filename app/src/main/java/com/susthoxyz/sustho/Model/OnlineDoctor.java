package com.susthoxyz.sustho.Model;

public class OnlineDoctor {

    private String doctorName,department,hospital,address,fee,degree, detail,startTime,endTime,timeType,doctorImage,available,doctorId ;

    public OnlineDoctor() {
    }

    public OnlineDoctor(String doctorName, String department, String hospital, String address, String fee, String degree, String detail, String startTime,
                        String endTime, String timeType, String doctorImage, String available, String doctorId) {
        this.doctorName = doctorName;
        this.department = department;
        this.hospital = hospital;
        this.address = address;
        this.fee = fee;
        this.degree = degree;
        this.detail = detail;
        this.startTime = startTime;
        this.endTime = endTime;
        this.timeType = timeType;
        this.doctorImage = doctorImage;
        this.available = available;
        this.doctorId = doctorId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public String getDoctorImage() {
        return doctorImage;
    }

    public void setDoctorImage(String doctorImage) {
        this.doctorImage = doctorImage;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }
}
