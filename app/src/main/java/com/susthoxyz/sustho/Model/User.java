package com.susthoxyz.sustho.Model;

public class User {

    String mobile , name , gender,blood,profileImage,payment, token;

    public User(String mobile, String name, String gender, String blood, String profileImage, String payment,String token) {
        this.mobile = mobile;
        this.name = name;
        this.gender = gender;
        this.blood = blood;
        this.profileImage = profileImage;
        this.payment = payment;
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
