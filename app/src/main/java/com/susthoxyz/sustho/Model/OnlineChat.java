package com.susthoxyz.sustho.Model;

public class OnlineChat {

    private String name,fee,time ;

    public OnlineChat() {
    }

    public OnlineChat(String name, String fee, String time) {
        this.name = name;
        this.fee = fee;
        this.time = time;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
