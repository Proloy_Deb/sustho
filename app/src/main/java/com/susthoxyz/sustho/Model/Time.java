package com.susthoxyz.sustho.Model;

public class Time {
    String time,sector ;

    public Time(String time,String sector) {
        this.time = time;
        this.sector = sector;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
