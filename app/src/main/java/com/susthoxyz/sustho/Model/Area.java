package com.susthoxyz.sustho.Model;

public class Area {
    String area ;

    public Area(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
