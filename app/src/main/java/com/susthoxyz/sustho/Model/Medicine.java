package com.susthoxyz.sustho.Model;

public class Medicine {

    private String home_address,address_details,area_name,medicine_qty,prescription_image,uploadId;

    public Medicine() {
    }

    public Medicine(String home_address, String address_details, String area_name, String medicine_qty, String prescription_image,String uploadId) {
        this.home_address = home_address;
        this.uploadId = uploadId ;
        this.address_details = address_details;
        this.area_name = area_name;
        this.medicine_qty = medicine_qty;
        this.prescription_image = prescription_image;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String getHome_address() {
        return home_address;
    }

    public void setHome_address(String home_address) {
        this.home_address = home_address;
    }

    public String getAddress_details() {
        return address_details;
    }

    public void setAddress_details(String address_details) {
        this.address_details = address_details;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getMedicine_qty() {
        return medicine_qty;
    }

    public void setMedicine_qty(String medicine_qty) {
        this.medicine_qty = medicine_qty;
    }

    public String getPrescription_image() {
        return prescription_image;
    }

    public void setPrescription_image(String prescription_image) {
        this.prescription_image = prescription_image;
    }
}
