package com.susthoxyz.sustho.Model;

public class Cart {

    private String prescription_image, uploadId ;

    public Cart() {
    }

    public Cart(String prescription_image, String uploadId) {
        this.prescription_image = prescription_image;
        this.uploadId = uploadId;
    }

    public String getPrescription_image() {
        return prescription_image;
    }

    public void setPrescription_image(String prescription_image) {
        this.prescription_image = prescription_image;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

}
