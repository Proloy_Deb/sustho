package com.susthoxyz.sustho.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.Fragment.Notification;
import com.susthoxyz.sustho.Model.Cart;
import com.susthoxyz.sustho.Model.Time;
import com.susthoxyz.sustho.Model.User;
import com.susthoxyz.sustho.R;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private Context mContext;
    private List<Notification> mData;
    SharedPreferences sharedPreferences ;
    String facebookId = null;

    public NotificationAdapter(List<Notification> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.layout_notification, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.MyViewHolder holder, final int position) {
        holder.tvHeader.setText(mData.get(position).getTitle());
        holder.tvDetail.setText(mData.get(position).getDetail());
        facebookId = mData.get(position).getLink();

        if(facebookId.equals("N/A"))
        {
            holder.tvLink.setVisibility(View.GONE);
        }

        else{
            holder.tvLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    facebookId = mData.get(position).getLink();

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookId));
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    mContext.getApplicationContext().startActivity(intent);

                    //  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookId)));
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvHeader,tvDetail,tvLink;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvHeader = itemView.findViewById(R.id.tvNotiTitle);
            tvDetail = itemView.findViewById(R.id.tvNotiDetail);
            tvLink = itemView.findViewById(R.id.tvNotiLink);
        }
    }
}