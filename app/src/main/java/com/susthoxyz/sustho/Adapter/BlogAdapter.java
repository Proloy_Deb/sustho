package com.susthoxyz.sustho.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.susthoxyz.sustho.Blog.BlogActivity;
import com.susthoxyz.sustho.Model.Blog;
import com.susthoxyz.sustho.R;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class BlogAdapter extends RecyclerView.Adapter<BlogAdapter.MyViewHolder> {

    private Context mContext;
    private List<Blog> mData;
    SharedPreferences sharedPreferences ;

    public BlogAdapter(List<Blog> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_blog, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BlogAdapter.MyViewHolder holder, final int position) {
        holder.tvTitle.setText(mData.get(position).getBlogtitle());
        Picasso.get().load(mData.get(position).getBlogimage()).into(holder.imvBlog);

        holder.parentBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String image = mData.get(position).getBlogimage();
                String title = mData.get(position).getBlogtitle();
                String detail = mData.get(position).getBlogdetail();

                sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("blogimage", image);
                editor.putString("blogtitle", title);
                editor.putString("blogdetail", detail);
                editor.commit();

                Intent myactivity = new Intent(mContext.getApplicationContext(), BlogActivity.class);
                myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.getApplicationContext().startActivity(myactivity);
            }
        });


    }



    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle ;
        private RelativeLayout parentBlog ;
        private ImageView imvBlog ;



        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvBlogTitle);
            parentBlog = itemView.findViewById(R.id.parentBlog);
            imvBlog = itemView.findViewById(R.id.imvBlogTitle);

        }
    }
}
