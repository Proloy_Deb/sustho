package com.susthoxyz.sustho.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.susthoxyz.sustho.Model.Cart;
import com.susthoxyz.sustho.R;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    private Context mContext;
    private List<Cart> mData;
    SharedPreferences sharedPreferences ;

    public CartAdapter(List<Cart> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_cart, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CartAdapter.MyViewHolder holder, final int position) {
        holder.tvOrderNumber.setText(mData.get(position).getUploadId());
  //      Picasso.get().load(mData.get(position).getImage()).into(holder.imvCartPrescribtion);
        Glide.with(mContext)
                .asBitmap()
                .load(mData.get(position).getPrescription_image())
                .into(holder.imvCartPrescribtion);

        String image = mData.get(position).getPrescription_image();
        holder.parentCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String image = mData.get(position).getPrescription_image();
                String quantity = mData.get(position).getUploadId();
                sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("orderimage", image);
                editor.putString("orderquantity", quantity);
                editor.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvOrderNumber;
        RelativeLayout parentCart ;
        ImageView imvCartPrescribtion ;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvOrderNumber = itemView.findViewById(R.id.tvOrderNumber);
            parentCart = itemView.findViewById(R.id.parentCart);
            imvCartPrescribtion = itemView.findViewById(R.id.imvOrderPrescribtion);
        }
    }
}