package com.susthoxyz.sustho.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.Activity.OnlineChatActivity;
import com.susthoxyz.sustho.Activity.RegistrationActivity;
import com.susthoxyz.sustho.Appointment.DoctorDetail;
import com.susthoxyz.sustho.Fragment.PaymentFragment;
import com.susthoxyz.sustho.Model.Cart;
import com.susthoxyz.sustho.Model.OnlineChat;
import com.susthoxyz.sustho.R;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ChatDoctorAdapter extends RecyclerView.Adapter<ChatDoctorAdapter.MyViewHolder> {

    private Context mContext;
    private List<OnlineChat> mData;
    private SharedPreferences sharedPreferences ;
    private String regMobile = null;

    public ChatDoctorAdapter(List<OnlineChat> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_chat, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChatDoctorAdapter.MyViewHolder holder, final int position) {
        holder.tvName.setText(mData.get(position).getName());
        holder.tvTime.setText(mData.get(position).getTime());
        holder.tvFee.setText(mData.get(position).getFee());
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String department = mData.get(position).getName();
                sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                regMobile = sharedPreferences.getString("mobile",null);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("onlinedepartment", department);
                editor.commit();

                if(regMobile != null)
                {
                    Intent myactivity = new Intent(mContext.getApplicationContext(), OnlineChatActivity.class);
                    myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    mContext.getApplicationContext().startActivity(myactivity);
                }

                else {
                    Intent myactivity = new Intent(mContext.getApplicationContext(), RegistrationActivity.class);
                    myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    mContext.getApplicationContext().startActivity(myactivity);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName,tvTime,tvFee;
        RelativeLayout parentLayout ;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvDoctorSpecialist);
            tvTime = itemView.findViewById(R.id.tvOnlineTime);
            tvFee = itemView.findViewById(R.id.tvFee);
            parentLayout = itemView.findViewById(R.id.parentDepartment);
        }
    }
}