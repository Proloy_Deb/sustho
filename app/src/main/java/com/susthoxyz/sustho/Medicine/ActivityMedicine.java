package com.susthoxyz.sustho.Medicine;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.susthoxyz.sustho.Activity.MainActivity;
import com.susthoxyz.sustho.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class ActivityMedicine extends AppCompatActivity {

    private Button btnCamera ;
    private String encode ,medicineQuantity;
    int Image_Request_Code = 999;
    int Image_Request_Gallery = 998 ;
    Uri FilePathUri;
    EditText edittTxt;
    Bitmap bitmap ;
    private CardView cvPrescribtion , cvPrescribtionImage;
    private Button btnSubmitPrescribtion,btnUploadGallery ;
    private ImageView imvPrescribtion ;
    private TextView tvPrescribtionTitle ;
    private RelativeLayout parentLayout;
    private EditText etMedicineQuantity ;
    SharedPreferences sharedPreferences ;
    private Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine);

        sharedPreferences = getApplicationContext().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);

        toolbar = findViewById(R.id.toolBarPrescribtion);
        btnCamera = findViewById(R.id.btnCamera);
        etMedicineQuantity = findViewById(R.id.etMedicineQuantity);
        imvPrescribtion = findViewById(R.id.imvPrescribtion);
        parentLayout = findViewById(R.id.parentl);
        btnSubmitPrescribtion = findViewById(R.id.btnPrescribtionPost);
        btnUploadGallery = findViewById(R.id.btnUploadGallery);
        cvPrescribtion = findViewById(R.id.cvPrescribtion);
        cvPrescribtionImage = findViewById(R.id.cvPrescribtionImage);
        tvPrescribtionTitle = findViewById(R.id.tvPrescribtionTitle);
        cvPrescribtionImage.setVisibility(View.GONE);
        btnSubmitPrescribtion.setVisibility(View.GONE);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(intent, Image_Request_Code);

                cvPrescribtion.setVisibility(View.GONE);
                tvPrescribtionTitle.setVisibility(View.GONE);
                cvPrescribtionImage.setVisibility(View.VISIBLE);
                btnSubmitPrescribtion.setVisibility(View.VISIBLE);
            }
        });

        btnUploadGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), Image_Request_Gallery);

                cvPrescribtion.setVisibility(View.GONE);
                tvPrescribtionTitle.setVisibility(View.GONE);
                cvPrescribtionImage.setVisibility(View.VISIBLE);
                btnSubmitPrescribtion.setVisibility(View.VISIBLE);
            }
        });

        btnSubmitPrescribtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicineQuantity = etMedicineQuantity.getText().toString();

                if(medicineQuantity.length()==0)
                {
                    etMedicineQuantity.setError("ফিল আপ করুন");
                }
                else {

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("encodeImage", encode);
                    editor.putString("medicineQuantity",medicineQuantity);
                    editor.commit();

                    AddressFragment addressFragment = new AddressFragment();
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.framePrescribtion,addressFragment).addToBackStack(null);
                    fragmentTransaction.commit();
                }

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code) {

                bitmap = (Bitmap)data.getExtras().get("data");
                imvPrescribtion.setImageBitmap(bitmap);
                getImageUri(getApplicationContext(),bitmap);
       //         imageToString(FilePathUri);
        }

        if (requestCode == Image_Request_Gallery && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), FilePathUri);
                imvPrescribtion.setImageBitmap(bitmap);
                imageToString(FilePathUri);
            }
            catch (IOException e) {

                e.printStackTrace();
            }
        }

    }
    private String imageToString(Uri bitmap)
    {
        /*ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
        byte[]image = outputStream.toByteArray();

        encode = Base64.encodeToString(image,Base64.DEFAULT);
        return encode;*/

        encode = bitmap.toString();
        return encode ;
    }

    public String getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        encode = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return encode;
    }

}
