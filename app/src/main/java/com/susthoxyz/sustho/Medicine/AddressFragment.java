package com.susthoxyz.sustho.Medicine;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.susthoxyz.sustho.Activity.LoginActivity;
import com.susthoxyz.sustho.Activity.MainActivity;
import com.susthoxyz.sustho.Activity.TestingActivity;
import com.susthoxyz.sustho.Model.Medicine;
import com.susthoxyz.sustho.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class AddressFragment extends Fragment {

    Spinner sp ;
    String spinnerUrl = "http://api.sustho.xyz/sustho/get_area.php";
    ArrayList<String> area;

    private String areakey = "give_all_area";
    private String medicineKey = "need_medicine";

    private ProgressDialog progressDialog ;

    private String postUrl = "http://api.sustho.xyz/sustho/user_prescription.php";
    private String encodeImage ,streetAddress , addressDetail,areaName,medicineQuantity,user_id,token,mobile;
    private EditText etstreet,etAddressDetail ;
    private Button btnNext ;

    Uri path ;
    StorageReference storageReference;
    DatabaseReference databaseReference ;
    SharedPreferences sharedPreferences ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_address, container, false);

        sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        encodeImage = sharedPreferences.getString("encodeImage",null);
        medicineQuantity = sharedPreferences.getString("medicineQuantity",null);
        user_id = sharedPreferences.getString("user_id",null);
        mobile = sharedPreferences.getString("mobile",null);
        token = sharedPreferences.getString("token",null);

        storageReference = FirebaseStorage.getInstance().getReference("prescribtion");
        databaseReference = FirebaseDatabase.getInstance().getReference("MedicineOrder").child(mobile);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCanceledOnTouchOutside(false);

        area = new ArrayList<>();
        sp = rootView.findViewById(R.id.spinnerArea);
        btnNext = rootView.findViewById(R.id.btnMedAddressNext);
        etstreet = rootView.findViewById(R.id.etHomeAddress);
        etAddressDetail = rootView.findViewById(R.id.etAddressDetail);
        loadSpinnerData(spinnerUrl);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                areaName =  sp.getItemAtPosition(sp.getSelectedItemPosition()).toString();
                Toast.makeText(getActivity(),areaName,Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                streetAddress = etstreet.getText().toString();
                addressDetail = etAddressDetail.getText().toString();

                if(streetAddress.length()==0)
                {
                    etstreet.setError("ফিল আপ করুন");
                }

                else {
                    postPrescribtion();
                }

            }
        });

        return rootView;
    }

    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }
    private void postPrescribtion() {

        progressDialog.setTitle("অপেক্ষা করুন. . .");
        progressDialog.show();

        path = Uri.parse(encodeImage);

        if(encodeImage != null)
        {
            final StorageReference storage = storageReference.child(System.currentTimeMillis()+"."+GetFileExtension(path));
            storage.putFile(path)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String url = uri.toString();
                                    String ImageUploadId = databaseReference.push().getKey();
                                    Medicine imageUploadInfo = new Medicine(streetAddress,addressDetail,areaName,medicineQuantity,url,ImageUploadId);
                                    databaseReference.child(ImageUploadId).setValue(imageUploadInfo);

                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Uploaded Successfully ", Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(getActivity(),MainActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    });
        }
    }

//    private void postPrescribtion() {
//
//        progressDialog.setTitle("অপেক্ষা করুন. . .");
//        progressDialog.show();
//
//        StringRequest request = new StringRequest(Request.Method.POST, postUrl,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            String status = jsonObject.getString("status");
//
//                            if (status.equals("1"))
//                            {
//                                progressDialog.dismiss();
//                                Toast.makeText(getActivity(),"Scucessfully Order",Toast.LENGTH_SHORT).show();
//                            }
//
//                        }
//                        catch (Exception e){
//                            e.printStackTrace();
//                            progressDialog.dismiss();
//                            Toast.makeText(getActivity(),"Error" + e.toString(),Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Toast.makeText(getActivity(),"Register Error" + error.toString(),Toast.LENGTH_SHORT).show();
//            }
//        })
//        {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String , String> params = new HashMap<>();
//                params.put("user_id",user_id);
//                params.put("area_name",areaName);
//                params.put("token",token);
//                params.put("prescription_image",encodeImage);
//                params.put("home_address",streetAddress);
//                params.put("address_details",addressDetail);
//                params.put("medicine_qty",medicineQuantity);
//                params.put("prescription",medicineKey);
//
//                return params;
//            }
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//        requestQueue.add(request);
//    }

    private void loadSpinnerData(String url) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("area");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String area_name = dataobj.getString("area_name");
                                area.add(area_name);
                            }
                            sp.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, area));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("token",token);
                params.put("area",areakey);

                return params;
            }
        };

        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }
}
