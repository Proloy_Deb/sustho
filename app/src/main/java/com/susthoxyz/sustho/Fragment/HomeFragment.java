package com.susthoxyz.sustho.Fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.susthoxyz.sustho.Activity.OnlineDoctorActivity;
import com.susthoxyz.sustho.Activity.RegistrationActivity;
import com.susthoxyz.sustho.Adapter.BlogAdapter;
import com.susthoxyz.sustho.Appointment.Appointment;
import com.susthoxyz.sustho.Appointment.AppointmentActivity;
import com.susthoxyz.sustho.Blog.ActivityBlogList;
import com.susthoxyz.sustho.Model.Blog;
import com.susthoxyz.sustho.OnlineQuestion.QuestionAnswer;
import com.susthoxyz.sustho.Appointment.AppointmentAdapter;
import com.susthoxyz.sustho.Adapter.SliderAdapterExample;
import com.susthoxyz.sustho.Medicine.ActivityMedicine;
import com.susthoxyz.sustho.PrescribtionBook.ActivityPrescirbtionBook;
import com.susthoxyz.sustho.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HomeFragment extends Fragment {

    private String url = "http://api.sustho.xyz/sustho/get_user_book_appointment.php";
    private String key = "give_book_appointment" ;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;

    private SliderView sliderView ;
    private List<Appointment> appointments;
    private RecyclerView rvappointments,rvBlog ;
    private CardView cvQuestionAnswer,cvDoctorConsult,cvAddPrescribtion,cvBlog ;
    private ImageView imvOnlineDoctor ;
    private AppointmentAdapter appointmentAdapter ;
    private SharedPreferences sharedPreferences ;
    private String regMobile = null ;
//    private String id = null ;
    private String token = null ;
    private int Storage_Permission_Code = 1;

    private BlogAdapter blogAdapter ;
    private ArrayList<Blog> blogs;
    private DatabaseReference reference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);

        regMobile = sharedPreferences.getString("mobile",null);
//        id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);

        reference = FirebaseDatabase.getInstance().getReference().child("blog");

        sliderView = rootView.findViewById(R.id.imageSlider);
//        cvDiagnostic = rootView.findViewById(R.id.Diagnostic);
        cvQuestionAnswer = rootView.findViewById(R.id.onlineQuestion);
        cvDoctorConsult = rootView.findViewById(R.id.DoctorConsult);
        rvappointments = rootView.findViewById(R.id.rvAppointment);
        cvBlog = rootView.findViewById(R.id.cvBlog);
        rvBlog = rootView.findViewById(R.id.rvBlog);
        cvAddPrescribtion = rootView.findViewById(R.id.addPrescribtion);
        imvOnlineDoctor = rootView.findViewById(R.id.imvOnlineChatting);
       /* sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token",null);*/
        SliderAdapterExample adapter = new SliderAdapterExample(getActivity());

        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.startAutoCycle();

        cvBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityBlogList.class);
                startActivity(intent);
            }
        });

        cvDoctorConsult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(token != null)
//                {
                  /*  Intent intent = new Intent(getActivity(), AppointmentActivity.class);
                    startActivity(intent);*/

                Toast.makeText(getActivity(), " সার্ভিসটি শীঘ্রই শুরু হবে ", Toast.LENGTH_SHORT).show();

//                }
/*
                else {

                    Toast.makeText(getActivity(), "ডাক্তার আ্যপয়েন্টমেন্ট নিতে রেজিস্ট্রেশন করুন", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                    startActivity(intent);

                }*/
            }
        });

    /*    cvDiagnostic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), " সার্ভিসটি শীঘ্রই শুরু হবে ", Toast.LENGTH_SHORT).show();
            }
        });*/

        cvQuestionAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), QuestionAnswer.class);
                startActivity(intent);
            }
        });

        imvOnlineDoctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), QuestionAnswer.class);
                startActivity(intent);
            }
        });

        cvAddPrescribtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ActivityPrescirbtionBook.class);
                startActivity(intent);
                /*if(token != null)
                {

                    if (ContextCompat.checkSelfPermission(getActivity()
                            , Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){

                        Intent intent = new Intent(getActivity(), ActivityMedicine.class);
                        startActivity(intent);
                    }
                    else {
                        checkPermission();
                    }

                }

                else {
                    Toast.makeText(getActivity(), "মেডিসিন অর্ডার করতে রেজিস্ট্রেশন করুন", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), RegistrationActivity.class);
                    startActivity(intent);
                }*/
            }
        });

        appointments = new ArrayList<>();
        appointmentAdapter = new AppointmentAdapter(appointments,getActivity());

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(rvappointments.getContext(), linearLayoutManager.getOrientation());

        rvBlog.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));

        rvappointments.setHasFixedSize(true);
        rvappointments.setLayoutManager(linearLayoutManager);
        rvappointments.addItemDecoration(dividerItemDecoration);
        rvappointments.setAdapter(appointmentAdapter);

/*
       if(id != null)
       {
           appointments();
       }*/

        blog();

        return rootView;
    }


    public void checkPermission()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.READ_EXTERNAL_STORAGE))
        {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Permission needed")
                    .setMessage("মেডিসিন এর জন্য প্রেসক্রিপশনের ছবি দিতে হবে")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, Storage_Permission_Code);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();
        }
        else {
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},Storage_Permission_Code);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Storage_Permission_Code)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Permission GRANTED", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ActivityMedicine.class);
                startActivity(intent);

            } else {
                Toast.makeText(getActivity(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void blog() {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                blogs = new ArrayList<Blog>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Blog blog = dataSnapshot1.getValue(Blog.class);
                    blogs.add(blog);

                }
                blogAdapter = new BlogAdapter( blogs,getActivity());
                rvBlog.setAdapter(blogAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*private void appointments() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("get_book_appointment");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String doctorName = dataobj.getString("doctor_name");
                                String department = dataobj.getString("doctor_department_name");
                                String hospital = dataobj.getString("doctor_hospital_name");
                                String time = dataobj.getString("book_appointment_time");

                                Appointment appointment = new Appointment(doctorName,hospital,department,time);
                                appointments.add(appointment);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        appointmentAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("user_id",id);
                params.put("book_appointment",key);

                return params;
            }
        };

        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }*/
}
