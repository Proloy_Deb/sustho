package com.susthoxyz.sustho.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Activity.OtpActivity;
import com.susthoxyz.sustho.Adapter.NotificationAdapter;
import com.susthoxyz.sustho.Appointment.AppointmentActivity;
import com.susthoxyz.sustho.Appointment.OnlineDoctorAdapter;
import com.susthoxyz.sustho.Model.OnlineDoctor;
import com.susthoxyz.sustho.Model.PrescribtionBook;
import com.susthoxyz.sustho.Model.Time;
import com.susthoxyz.sustho.Model.User;
import com.susthoxyz.sustho.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NotificationFragment extends Fragment {

    private RecyclerView rvNoti ;
    private LinearLayoutManager linearLayoutManager;
    private List<Notification> notifications;
    private NotificationAdapter notificationAdapter ;

    private String url = "http://api.sustho.xyz/sustho/get_user_notification.php";
    private String key = "give_notification" ;
    private String id,token,mobile ;
    private SharedPreferences sharedPreferences ;
    private DatabaseReference reference ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);
        sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);
        mobile = sharedPreferences.getString("mobile",null);

        reference = FirebaseDatabase.getInstance().getReference().child("Notification");
        rvNoti = rootView.findViewById(R.id.rvNotification);

        rvNoti.setLayoutManager(new LinearLayoutManager(getActivity()));

        /*linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvNoti.setHasFixedSize(true);
        rvNoti.setLayoutManager(linearLayoutManager);
        rvNoti.setAdapter(notificationAdapter);*/

//        get_notifications();

        return rootView;
    }

    private void get_notifications() {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                notifications = new ArrayList<Notification>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    if((dataSnapshot1.child("mobile").getValue().toString()).equals(mobile))
                    {
                        Notification notification = dataSnapshot1.getValue(Notification.class);
                        notifications.add(notification);
                    }

                }
                notificationAdapter = new NotificationAdapter( notifications, getActivity());
                rvNoti.setAdapter(notificationAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*private void get_notifications() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("get_notification");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String title = dataobj.getString("user_notification_title");
                                String details = dataobj.getString("user_notification_details");

                                Time time = new Time(title,details);
                                notifications.add(time);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        notificationAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("user_id", id);
                params.put("token",token);;
                params.put("get_notification",key);

                return params;
            }
        };

        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }*/
}
