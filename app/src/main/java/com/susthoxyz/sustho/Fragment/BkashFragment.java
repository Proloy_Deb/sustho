package com.susthoxyz.sustho.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.susthoxyz.sustho.Activity.MainActivity;
import com.susthoxyz.sustho.Activity.RegistrationActivity;
import com.susthoxyz.sustho.Model.User;
import com.susthoxyz.sustho.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class BkashFragment extends Fragment {

    private EditText etTransaction , etAmount ;
    private String transaction , amount ;
    private Button btnSusmit ;
    /*private String url = "http://api.sustho.xyz/sustho/user_payment.php" ;
    private String key = "post_payment";
    private String token = "dea55a4698f9c1283a37d0a9c3febdb5c9977ef5";
    private String id = "6";*/
    private SharedPreferences sharedPreferences ;
    private DatabaseReference reference ;
    private String mobile = null ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_bkash, container, false);

        reference = FirebaseDatabase.getInstance().getReference().child("Payment");
        sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        mobile = sharedPreferences.getString("mobile",null);

        etTransaction = rootView.findViewById(R.id.etTransitionId);
        etAmount = rootView.findViewById(R.id.etBkashTk);
        btnSusmit = rootView.findViewById(R.id.btnBkashSubmit);

        btnSusmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postpayment();
            }
        });
        return rootView ;
    }

    private void postpayment() {

        transaction = etTransaction.getText().toString();
        amount = etAmount.getText().toString();

        if (transaction.isEmpty())
        {
            etTransaction.setError("ফিল - আপ করুন");
        }

        if (amount.isEmpty())
        {
            etAmount.setError("ফিল - আপ করুন");
        }

        else {
            String parentKey = reference.push().getKey();

            Payment payment = new Payment(transaction,amount,mobile,parentKey);

            reference.child(parentKey).setValue(payment).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    Toast.makeText(getActivity(), " এক ঘণ্টার ভিতর পেমেন্টটি আপডেট হয়ে যাবে ", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(),MainActivity.class);
                    startActivity(intent);
                }
            });
        }

    }

   /* private void postpayment() {

        transaction = etTransaction.getText().toString();
        amount = etAmount.getText().toString();
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals("1"))
                            {
                                Toast.makeText(getActivity(),"Register Scucessfully"+status,Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(getActivity(),status,Toast.LENGTH_SHORT).show();

                            }

                        }
                        catch (Exception e){
                            e.printStackTrace();
                            Toast.makeText(getActivity(),"Error" + e.toString(),Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Register Error" + error.toString(),Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> params = new HashMap<>();
                params.put("user_id",id);
                params.put("token",token);
                params.put("transitionNo",transaction);
                params.put("fees",amount);
                params.put("payment",key);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);
    }*/
}
