package com.susthoxyz.sustho.Fragment;

public class Notification {

    private String title,detail,parentkey,link ;

    public Notification() {
    }

    public Notification(String title, String detail,String parentkey,String link) {
        this.title = title;
        this.detail = detail;
        this.parentkey = parentkey ;
        this.link = link ;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getParentkey() {
        return parentkey;
    }

    public void setParentkey(String parentkey) {
        this.parentkey = parentkey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
