package com.susthoxyz.sustho.Fragment;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.susthoxyz.sustho.R;


public class PaymentFragment extends DialogFragment {

    private Button btnOk ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_payment, container, false);
        btnOk = rootView.findViewById(R.id.btnConfirmReferal);
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                BkashFragment bkashFragment = new BkashFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frameOnlineChat,bkashFragment);
                fragmentTransaction.commit();
            }
        });
        return rootView ;
    }
}
