package com.susthoxyz.sustho.Fragment;

public class Payment {
    private String transactionId,amount,userMobile,parentKey ;

    public Payment(String transactionId, String amount, String userMobile, String parentKey) {
        this.transactionId = transactionId;
        this.amount = amount;
        this.userMobile = userMobile;
        this.parentKey = parentKey;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getParentKey() {
        return parentKey;
    }

    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }
}
