package com.susthoxyz.sustho.PrescribtionBook;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.susthoxyz.sustho.Model.PrescribtionBook;
import com.susthoxyz.sustho.R;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyViewHolder> {

    private Context mContext;
    private List<PrescribtionBook> mData;
    SharedPreferences sharedPreferences ;

    public BookAdapter(List<PrescribtionBook> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_book, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BookAdapter.MyViewHolder holder, final int position) {
        holder.tvDate.setText(mData.get(position).getDate());
        holder.tvDesease.setText(mData.get(position).getProblem());

        holder.parentBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String image = mData.get(position).getImage();
                sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("prescribtionimage", image);
                editor.commit();

                Intent myactivity = new Intent(mContext.getApplicationContext(), ShowPrescribtion.class);
                myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.getApplicationContext().startActivity(myactivity);


            }
        });
    }



    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate,tvDesease ;
        RelativeLayout parentBook ;
        ImageView imvPrescribtion ;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvBookDate);
            tvDesease = itemView.findViewById(R.id.tvProblem);
            parentBook = itemView.findViewById(R.id.parentBook);
            imvPrescribtion = itemView.findViewById(R.id.imvPrescribtionBook);
        }
    }
}