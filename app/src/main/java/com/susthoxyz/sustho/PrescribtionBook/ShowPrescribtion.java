package com.susthoxyz.sustho.PrescribtionBook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.R;

public class ShowPrescribtion extends AppCompatActivity {

    SharedPreferences sharedPreferences ;
    private ImageView imvPrescribton ;
    private String image ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_prescribtion);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        image = sharedPreferences.getString("prescribtionimage",null);
        imvPrescribton = findViewById(R.id.imvShowPrescribtion);

        Picasso.get().load(image).into(imvPrescribton);
    }
}
