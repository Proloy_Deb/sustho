package com.susthoxyz.sustho.PrescribtionBook;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Adapter.CartAdapter;
import com.susthoxyz.sustho.Model.Cart;
import com.susthoxyz.sustho.Model.PrescribtionBook;
import com.susthoxyz.sustho.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityPrescirbtionBook extends AppCompatActivity {

    private RecyclerView rvBook ;
    private LinearLayoutManager linearLayoutManager;
    private List<PrescribtionBook> prescribtionBooks;
    private SharedPreferences sharedPreferences ;
    private BookAdapter bookAdapter ;
    private Button btnNewPrescribtion ;
    private Toolbar toolbar ;

    private DatabaseReference reference ;
    private String url = "http://api.sustho.xyz/sustho/get_user_prescription_book.php";
    private String key = "give_prescription_book_info" ;
    private String id = null ;
    private String token = null ;
    private String mobile = null ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescirbtion_book);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);
        mobile = sharedPreferences.getString("mobile",null);

        reference = FirebaseDatabase.getInstance().getReference().child("PrescribtionBook");
        rvBook = findViewById(R.id.rvBook);
        toolbar = findViewById(R.id.toolBarBook);
        btnNewPrescribtion = findViewById(R.id.btnNewPrescribtion);

        rvBook.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
/*
        prescribtionBooks = new ArrayList<>();
        bookAdapter = new BookAdapter(prescribtionBooks,getApplicationContext());

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvBook.setHasFixedSize(true);
        rvBook.setLayoutManager(linearLayoutManager);
        rvBook.setAdapter(bookAdapter);*/

        btnNewPrescribtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(token != null)
                {
                    Intent intent = new Intent(getApplicationContext(),AddNewPrescribtIon.class);
                    startActivity(intent);
                }

                else {
                    Toast.makeText(ActivityPrescirbtionBook.this, "রেজিস্ট্রেশন / লগ-ইন করুন ", Toast.LENGTH_SHORT).show();
                }

            }
        });

        if(token != null)
        {
            Prescribtion_List();
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void Prescribtion_List() {

        reference.child(mobile).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                prescribtionBooks = new ArrayList<PrescribtionBook>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    PrescribtionBook prescribtionBook = dataSnapshot1.getValue(PrescribtionBook.class);
                    prescribtionBooks.add(prescribtionBook);

                }
                bookAdapter = new BookAdapter( prescribtionBooks,getApplicationContext());
                rvBook.setAdapter(bookAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

/*    private void Prescribtion_List() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("get_book_prescription");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String image = dataobj.getString("prescription_book_image");
                                String problemName = dataobj.getString("prescription_book_doctor_name");
                                String book_date = dataobj.getString("prescription_book_date");

                                PrescribtionBook prescribtionBook = new PrescribtionBook(book_date,image,problemName);
                                prescribtionBooks.add(prescribtionBook);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ActivityPrescirbtionBook.this, "কোন প্রেসক্রিপশন নেই", Toast.LENGTH_SHORT).show();
                        }

                        bookAdapter.notifyDataSetChanged();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("user_id", id);
                params.put("token",token);;
                params.put("prescription_book",key);

                return params;
            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }*/

}
