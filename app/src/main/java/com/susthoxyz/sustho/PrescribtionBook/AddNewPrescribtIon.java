package com.susthoxyz.sustho.PrescribtionBook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.susthoxyz.sustho.Activity.MainActivity;
import com.susthoxyz.sustho.Fragment.DatePickerFragment;
import com.susthoxyz.sustho.Model.Cart;
import com.susthoxyz.sustho.Model.Medicine;
import com.susthoxyz.sustho.Model.PrescribtionBook;
import com.susthoxyz.sustho.R;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddNewPrescribtIon extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private EditText tvDate ;
    private Button btnPrescirbtion,btnUpload ;
    private EditText etDeseaseName ;
    private String url = "http://api.sustho.xyz/sustho/user_prescription_book.php";
    private String key = "book_prescription";

    private ProgressDialog progressDialog ;
    private ImageView imvPrescribtion,imvCalender ;
    private SharedPreferences sharedPreferences ;
    private String encode ,currentDateFormat,deseaseName,user_id,token;
    private String mobile = null ;
    private StorageReference storageReference ;
    private DatabaseReference reference ;
    int Image_Request_Code = 999;
    Uri FilePathUri;
    Bitmap bitmap ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_prescribt_ion);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);
        mobile = sharedPreferences.getString("mobile",null);

        storageReference = FirebaseStorage.getInstance().getReference("prescribtion");
        reference = FirebaseDatabase.getInstance().getReference("PrescribtionBook").child(mobile);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);

        btnPrescirbtion = findViewById(R.id.btnUploadPrescribtion);
        imvPrescribtion = findViewById(R.id.imvBookPrescribtion);
        imvCalender = findViewById(R.id.imvCalenderChoose);
        etDeseaseName = findViewById(R.id.etBookDocName);
        btnUpload = findViewById(R.id.btnBookSubmit);
        tvDate = findViewById(R.id.tvCalender);

        imvCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");

            }
        });

        btnPrescirbtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), Image_Request_Code);
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postPrescribtion();
            }
        });

    }

    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }

    private void postPrescribtion() {

        deseaseName = etDeseaseName.getText().toString();
        currentDateFormat = tvDate.getText().toString();

        progressDialog.setTitle("অপেক্ষা করুন. . .");
        progressDialog.show();

        if(FilePathUri != null)
        {
            final StorageReference storage = storageReference.child(System.currentTimeMillis()+"."+GetFileExtension(FilePathUri));
            storage.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String url = uri.toString();
                                    String ImageUploadId = reference.push().getKey();
                                    PrescribtionBook prescribtionBook = new PrescribtionBook(currentDateFormat,url,deseaseName,ImageUploadId);
                                    reference.child(ImageUploadId).setValue(prescribtionBook);

                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Uploaded Successfully ", Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    });
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar =  Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month);
        calendar.set(Calendar.DATE,dayOfMonth);
        currentDateFormat = DateFormat.getDateInstance().format(calendar.getTime());
        tvDate.setText(currentDateFormat);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), FilePathUri);
                imvPrescribtion.setImageBitmap(bitmap);
            }
            catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

  /*  private String imageToString(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
        byte[]image = outputStream.toByteArray();

        encode = Base64.encodeToString(image,Base64.DEFAULT);
        return encode;
    }*/

//    private void postPrescribtion() {
//
//        deseaseName = etDeseaseName.getText().toString();
//        currentDateFormat = tvDate.getText().toString();
//
//        progressDialog.setTitle("অপেক্ষা করুন. . .");
//        progressDialog.show();
//
//        StringRequest request = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            String status = jsonObject.getString("status");
//
//                            if (status.equals("1"))
//                            {
//                                Toast.makeText(getApplicationContext(),"Scucessfully Post",Toast.LENGTH_SHORT).show();
//                                progressDialog.dismiss();
//                            }
//
//                        }
//                        catch (Exception e){
//                            e.printStackTrace();
//                            progressDialog.dismiss();
//                            Toast.makeText(getApplicationContext(),"Error" + e.toString(),Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(),"Error" + error.toString(),Toast.LENGTH_SHORT).show();
//            }
//        })
//        {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String , String> params = new HashMap<>();
//                params.put("user_id",user_id);
//                params.put("token",token);
//                params.put("book_image",encode);
//                params.put("doctor_name",deseaseName);
//                params.put("book_date",currentDateFormat);
//                params.put("prescription",key);
//
//                return params;
//            }
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
//        requestQueue.add(request);
//    }
}
