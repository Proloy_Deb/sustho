package com.susthoxyz.sustho.Profile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.Activity.MainActivity;
import com.susthoxyz.sustho.Model.PrescribtionBook;
import com.susthoxyz.sustho.Model.User;
import com.susthoxyz.sustho.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    /*private String url = "http://api.sustho.xyz/sustho/user_update.php";
    private String key = "update_user";
    private String token = null;
    private String id = null;*/

    private TextView tvName , tvGender , tvBlood,tvMobile ;
    private Toolbar toolbar;
    private CircularImageView cvProfile ;
    private EditText etName ;
    private Spinner spGender , spBlood ;
    private RadioGroup rgGender ;
    private String gender,blood,name,image,mobile ;
    private Button btnProfileUpdate ;
    private ImageView imvEdit ;
    private SharedPreferences sharedPreferences ;
    private DatabaseReference reference ;
    private StorageReference storageReference ;
    private ProgressDialog progressDialog ;
    private String payment = "0";
    private String token = null;
    int Image_Request_Code = 999;
    Uri FilePathUri;
    private int change = 0;
    private int profileSelect = 0 ;

    Bitmap bitmap ;
    private String encode ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        toolbar = findViewById(R.id.toolBarProfile);

        storageReference = FirebaseStorage.getInstance().getReference("prescribtion");
        reference = FirebaseDatabase.getInstance().getReference("Users");

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
/*
        id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);*/

        name = sharedPreferences.getString("name",null);
        token = sharedPreferences.getString("token",null);
        gender = sharedPreferences.getString("gender",null);
        blood = sharedPreferences.getString("blood",null);
        image = sharedPreferences.getString("image",null);
        mobile = sharedPreferences.getString("mobile",null);

        tvName = findViewById(R.id.tvName);
        tvGender = findViewById(R.id.tvGender);
        tvBlood = findViewById(R.id.tvBlood);
        tvMobile = findViewById(R.id.tvMobile);
        etName = findViewById(R.id.etName);
        imvEdit = findViewById(R.id.imvEdit);
        spGender = findViewById(R.id.spinnerGender);
        spBlood = findViewById(R.id.spinnerBlood);
        rgGender = findViewById(R.id.rgGenderUpdate);
        btnProfileUpdate = findViewById(R.id.btnUpdate);
        cvProfile = findViewById(R.id.imvProfilePicture);

        etName.setVisibility(View.GONE);
        spGender.setVisibility(View.GONE);
        spBlood.setVisibility(View.GONE);
        btnProfileUpdate.setVisibility(View.GONE);
        rgGender.setVisibility(View.GONE);

        tvName.setText(name);
        tvGender.setText(gender);
        tvBlood.setText(blood);
        tvMobile.setText(mobile);
        Picasso.get().load(image).into(cvProfile);

        imvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileSelect = 1;
                etName.setVisibility(View.VISIBLE);
                spBlood.setVisibility(View.VISIBLE);
                btnProfileUpdate.setVisibility(View.VISIBLE);
                rgGender.setVisibility(View.VISIBLE);
                etName.setText(name);

                tvName.setVisibility(View.GONE);
                tvGender.setVisibility(View.GONE);
                tvBlood.setVisibility(View.GONE);
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);

        ArrayAdapter<CharSequence> adapterGender = ArrayAdapter.createFromResource(getApplicationContext(),R.array.blood,android.R.layout.simple_spinner_item);
        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBlood.setPrompt("Gender");
        spBlood.setAdapter(adapterGender);
        spBlood.setOnItemSelectedListener(this);

        btnProfileUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = etName.getText().toString();
                update();
            }
        });

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = findViewById(checkedId);
                gender = checkedRadioButton.getText().toString();
            }
        });

        cvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (profileSelect == 1)

                {
                    change = 1;
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Image"), Image_Request_Code);
                }

            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    /*private void update() {

        name = etName.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")){

                                Toast.makeText(ProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("user_id", id);
                params.put("token",token);
                params.put("username",name);
                params.put("blood_group",blood);
                params.put("gender",gender);
                params.put("profile_image",encode);
                params.put("update",key);

                return params;
            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }*/

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        blood =  parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), FilePathUri);
                cvProfile.setImageBitmap(bitmap);
            //    imageToString(bitmap);
            }
            catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

   /* private String imageToString(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
        byte[]image = outputStream.toByteArray();

        encode = Base64.encodeToString(image,Base64.DEFAULT);
        return encode;
    }*/

    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }

    private void update()
    {
        progressDialog.setTitle("অপেক্ষা করুন. . .");
        progressDialog.show();


        if(change == 0)
        {
            User user = new User(mobile,name,gender,blood,image,payment,token);
            reference.child(mobile).setValue(user);

            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Uploaded Successfully ", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }

        else {

            if(FilePathUri != null)
            {
                final StorageReference storage = storageReference.child(System.currentTimeMillis()+"."+GetFileExtension(FilePathUri));
                storage.putFile(FilePathUri)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                storage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {

                                        image = uri.toString();
                                        User user = new User(mobile,name,gender,blood,image,payment,token);
                                        reference.child(mobile).setValue(user);

                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Uploaded Successfully ", Toast.LENGTH_LONG).show();

                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);


                                    }
                                });
                            }
                        });
            }
        }


    }
}
