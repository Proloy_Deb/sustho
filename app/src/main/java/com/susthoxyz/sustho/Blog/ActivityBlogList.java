package com.susthoxyz.sustho.Blog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.susthoxyz.sustho.Adapter.BlogAdapter;
import com.susthoxyz.sustho.Model.Blog;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;

public class ActivityBlogList extends AppCompatActivity {

    private BlogAdapter blogAdapter ;
    private ArrayList<Blog> blogs;
    private DatabaseReference reference;
    private RecyclerView rvBlogList;
    private Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_list);

        toolbar = findViewById(R.id.toolBarBlog);

        reference = FirebaseDatabase.getInstance().getReference().child("blog");
        rvBlogList = findViewById(R.id.rvBlogList);
        rvBlogList.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));

        blog();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void blog() {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                blogs = new ArrayList<Blog>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Blog blog = dataSnapshot1.getValue(Blog.class);
                    blogs.add(blog);

                }
                blogAdapter = new BlogAdapter( blogs,getApplicationContext());
                rvBlogList.setAdapter(blogAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}