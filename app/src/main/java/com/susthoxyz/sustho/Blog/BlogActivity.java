package com.susthoxyz.sustho.Blog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.R;

public class BlogActivity extends AppCompatActivity {

    private TextView tvTitle , tvDetail ;
    private ImageView imvBlog ;
    private SharedPreferences sharedPreferences ;
    private String image , title , detail ;
    private Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);

        tvTitle = findViewById(R.id.tvBlogDetailTitle);
        tvDetail = findViewById(R.id.tvBlogDetail);
        imvBlog = findViewById(R.id.imvBlogDetail);
        toolbar = findViewById(R.id.toolBarBlog);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       /*
        if (Build.VERSION.SDK_INT > 23) {


        } else {
            // Implement this feature without material design
        }*/

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        image = sharedPreferences.getString("blogimage",null);
        title = sharedPreferences.getString("blogtitle",null);
        detail = sharedPreferences.getString("blogdetail",null);

        tvTitle.setText(title);
        tvDetail.setText(detail);
        Picasso.get().load(image).into(imvBlog);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
