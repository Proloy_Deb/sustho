package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText etLoginMobie ;
    private Button btnLoginMobie ;
    SharedPreferences sharedPreferences ;
    private String mobile ;
    private DatabaseReference databaseReference ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etLoginMobie = findViewById(R.id.etLoginMobile);
        btnLoginMobie = findViewById(R.id.btnLogin);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users");

        btnLoginMobie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mobile = "+88"+etLoginMobie.getText().toString();

                if(mobile.length()==0)
                {
                    etLoginMobie.setError("মোবাইল ফিল আপ করুন");
                }

                else {
                    checkID(mobile);
                }

            }
        });
    }

    private void checkID(final String mobile) {

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    String check = dataSnapshot1.child("mobile").getValue().toString();
                    aa:

                    if(check.equals(mobile))
                    {

                        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("mobile",mobile);
                        editor.commit();

                        Intent intent = new Intent(getApplicationContext(),OtpActivity.class);
                        intent.putExtra("type","login");
                        startActivity(intent);
                        break aa;

                    }

                    else if(!check.equals(mobile)) {

                        etLoginMobie.setError("আগে রেজিস্ট্রেশন করুন");
                        break aa;

                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
