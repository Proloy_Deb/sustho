package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Adapter.CartAdapter;
import com.susthoxyz.sustho.Model.Cart;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;
import java.util.List;

public class CartActivity extends AppCompatActivity {

    private LinearLayoutManager linearLayoutManager;
    private List<Cart> carts;
    private CartAdapter cartAdapter ;
    private RecyclerView rvCarts ;

    private String url = "http://api.sustho.xyz/sustho/get_prescription_cart.php";
    private String token = null ;
    private String id = null ;
    private String mobile = null ;
    private String key = "give_prescription_info" ;
    private Toolbar toolbar ;
    private SharedPreferences sharedPreferences ;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);
        mobile = sharedPreferences.getString("mobile",null);

        reference = FirebaseDatabase.getInstance().getReference().child("MedicineOrder");

        rvCarts = findViewById(R.id.rvCart);
        toolbar = findViewById(R.id.toolBarCart);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*carts = new ArrayList<>();
        cartAdapter = new CartAdapter(carts,getApplicationContext());

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        rvCarts.setHasFixedSize(true);
        rvCarts.setLayoutManager(linearLayoutManager);
        rvCarts.setAdapter(cartAdapter);*/

        rvCarts.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

     /*   imvArrrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        })*/;

        if (id!=null)
        {
            get_cart();
        }

    }

    private void get_cart() {

        reference.child(mobile).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                carts = new ArrayList<Cart>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Cart cart = dataSnapshot1.getValue(Cart.class);
                    carts.add(cart);

                }
                cartAdapter = new CartAdapter( carts,getApplicationContext());
                rvCarts.setAdapter(cartAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

//    private void get_cart() {
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//
//                            JSONArray dataArray = jsonObject.getJSONArray("get_cart");
//
//                            for (int i = 0; i < dataArray.length(); i++) {
//
//                                JSONObject dataobj = dataArray.getJSONObject(i);
//
//                                String prescribtionImage = dataobj.getString("prescription_image");
//                                String serialNumber = dataobj.getString("prescription_serial_number");
//                                String medicineQuantity = dataobj.getString("prescription_medicine_quantity");
//
//                                Cart cart = new Cart(prescribtionImage,serialNumber,medicineQuantity);
//                                carts.add(cart);
//
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(getApplicationContext(), "কোন অর্ডার নেই", Toast.LENGTH_SHORT).show();
//                        }
//
//                        cartAdapter.notifyDataSetChanged();
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
//                params.put("user_id",id);
//                params.put("view_cart",key);
//
//                return params;
//            }
//        };
//
//        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
//    }
}
