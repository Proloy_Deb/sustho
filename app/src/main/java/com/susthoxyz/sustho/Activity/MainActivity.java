package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.susthoxyz.sustho.Appointment.AppointmentActivity;
import com.susthoxyz.sustho.Appointment.AppointmentFragment;
import com.susthoxyz.sustho.Appointment.OnlineDoctorAdapter;
import com.susthoxyz.sustho.Fragment.BkashFragment;
import com.susthoxyz.sustho.Fragment.HomeFragment;
import com.susthoxyz.sustho.Fragment.NotificationFragment;
import com.susthoxyz.sustho.Fragment.PaymentFragment;
import com.susthoxyz.sustho.Fragment.ProfileFragment;
import com.susthoxyz.sustho.Medicine.ActivityMedicine;
import com.susthoxyz.sustho.Model.OnlineDoctor;
import com.susthoxyz.sustho.PrescribtionBook.ActivityPrescirbtionBook;
import com.susthoxyz.sustho.Profile.ProfileActivity;
import com.susthoxyz.sustho.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private HomeFragment homeFragment ;
    private NotificationFragment notificationFragment ;
    private BottomNavigationView bottomNavigationView ;

    private NavigationView mNavigationView ;
    private DrawerLayout drawerLayout ;
    private ImageView imvMenu,imvCart ;
    private CircularImageView cvProfile ;
    private SharedPreferences sharedPreferences ;
    private TextView tvHaderName ;
    private DatabaseReference reference ;
    private String name = null;
//    private String id = null ;
    private String mobile = null ;
    private String token = null ;
    private String  gender , blood ,image  ;
    private CircularImageView imvHeaderProfile ;

   /* private String profileUrl = "http://api.sustho.xyz/sustho/get_user_profile.php";
    private String profileKey = "give_profile_info";*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);

        mobile = sharedPreferences.getString("mobile",null);
//        id = sharedPreferences.getString("user_id",null);
        token = sharedPreferences.getString("token",null);

        reference = FirebaseDatabase.getInstance().getReference().child("Users");

        bottomNavigationView = findViewById(R.id.bottomBar);
        imvCart = findViewById(R.id.imvCart);
        mNavigationView = findViewById(R.id.drawerNavigation);
        drawerLayout = findViewById(R.id.drawer);
        imvMenu = findViewById(R.id.imvMenu);
        cvProfile = findViewById(R.id.imvHomeProfile);


        NavigationView navigationView = (NavigationView) findViewById(R.id.drawerNavigation);
        View header = navigationView.getHeaderView(0);
        tvHaderName = header.findViewById(R.id.tvHeaderName);
        imvHeaderProfile = header.findViewById(R.id.menubarProfile);

        mNavigationView.setNavigationItemSelectedListener(this);
        homeFragment = new HomeFragment();
        notificationFragment = new NotificationFragment();
        setFragment(homeFragment);



        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home:
                        setFragment(homeFragment);
                        return true;
                    case R.id.write:

                        Intent intent = new Intent(MainActivity.this, ActivityPrescirbtionBook.class);
                        startActivity(intent);
                        return true;
                        /*if (id != null)
                        {
                            Intent intent = new Intent(MainActivity.this, ActivityMedicine.class);
                            startActivity(intent);
                        }
                        else {
                            Intent intent = new Intent(MainActivity.this, RegistrationActivity.class);
                            startActivity(intent);
                        }*/
                    case R.id.notification:
                        setFragment(notificationFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });

        imvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        imvCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);;
            }
        });

        cvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (token!=null)
                {
                    Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                    startActivity(intent);
                }

                else {

                    Toast.makeText(MainActivity.this, "রেজিস্ট্রেশন / লগ-ইন করুন ", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                    startActivity(intent);
                }

            }
        });

        if(mobile != null)
        {
            get_profile();
        }

        /*FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
//                        Log.d(TAG, msg);
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });*/

//        updateToken(FirebaseInstanceId.getInstance().getToken());

    }

   /* public void updateToken(String token)
    {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Tokens");
        Token token1 = new Token(token);
        reference.child(mobile).setValue(token1);
    }*/

    public void setFragment(Fragment fragment)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer,fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int menuId = menuItem.getItemId();

        if (menuId == R.id.nav_profile)
        {
            drawerLayout.closeDrawer(Gravity.LEFT);

            if (token!=null)
            {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
            }

            else {
                Toast.makeText(MainActivity.this, "রেজিস্ট্রেশন / লগ-ইন করুন ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                startActivity(intent);
            }
        }

        if (menuId == R.id.nav_payment)
        {
            drawerLayout.closeDrawer(Gravity.LEFT);
            BkashFragment paymentFragment = new BkashFragment();
            setFragment(paymentFragment);
        }

        if (menuId == R.id.nav_appointment)
        {
            drawerLayout.closeDrawer(Gravity.LEFT);
            Toast.makeText(this, "সার্ভিসটি শীঘ্রই শুরু হবে", Toast.LENGTH_SHORT).show();
        }

        if (menuId == R.id.nav_question)
        {
            if(token!=null)
            {
                Intent intent = new Intent(getApplicationContext(), OwnQuestionActivity.class);
                startActivity(intent);
            }

            else {
                Toast.makeText(MainActivity.this, "রেজিস্ট্রেশন / লগ-ইন করুন ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                startActivity(intent);
            }

        }

        if (menuId == R.id.nav_book)
        {
            Intent intent = new Intent(getApplicationContext(), ActivityPrescirbtionBook.class);
            startActivity(intent);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void get_profile()

    {
        reference.child(mobile).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {
                    name = dataSnapshot.child("name").getValue().toString();
                    mobile = dataSnapshot.child("mobile").getValue().toString();
                    gender = dataSnapshot.child("gender").getValue().toString();
                    blood = dataSnapshot.child("blood").getValue().toString();
                    token = dataSnapshot.child("token").getValue().toString();
                    image = dataSnapshot.child("profileImage").getValue().toString();


                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("name", name);
                    editor.putString("image", image);
                    editor.putString("blood", blood);
                    editor.putString("gender", gender);
                    editor.putString("token", token);
                    editor.commit();

                    tvHaderName.setText(name);
                    Picasso.get().load(image).into(imvHeaderProfile);
                    Picasso.get().load(image).into(cvProfile);
                }

                catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), " লগ-ইন করুন" , Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*private void get_profile() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, profileUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");

                            if (status.equals("1"))
                            {
                                JSONArray dataArray = jsonObject.getJSONArray("get_profile");

                                for (int i = 0; i < dataArray.length(); i++) {

                                    JSONObject dataobj = dataArray.getJSONObject(i);

                                    name = dataobj.getString("user_name");
                                    image = dataobj.getString("user_profile_image");
                                    blood = dataobj.getString("user_blood_group");
                                    gender = dataobj.getString("user_gender");
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("name", name);
                                    editor.putString("image", image);
                                    editor.putString("blood", blood);
                                    editor.putString("gender", gender);
                                    editor.commit();


                                    tvHaderName.setText(name);
                                    Picasso.get().load(image).into(imvHeaderProfile);
                                    Picasso.get().load(image).into(cvProfile);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                    params.put("user_id", id);
                    params.put("token",token);
                    params.put("get_profile",profileKey);

                return params;
            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }*/
}
