package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.OnlineQuestion.QuestionAdapter;
import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;

public class OwnQuestionActivity extends AppCompatActivity {

    private RecyclerView rvOwnQuestion ;
    QuestionAdapter questionAdapter ;
    ArrayList<Question> questions;
    private DatabaseReference reference;
    private SharedPreferences sharedPreferences ;
    private String mobile  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_own_question);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        mobile = sharedPreferences.getString("mobile",null);

        rvOwnQuestion = findViewById(R.id.rvOwnQuestion);
        reference = FirebaseDatabase.getInstance().getReference().child("QuestionAnswer");
        rvOwnQuestion.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        reference.child(mobile).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                questions = new ArrayList<Question>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    for (DataSnapshot dataSnapshot12 : dataSnapshot1.getChildren()) {

                            if((dataSnapshot12.child("type").getValue().toString()).equals("question")) {

                                Question question = dataSnapshot12.getValue(Question.class);
                                questions.add(question);
                            }
                    }
                }
                questionAdapter = new QuestionAdapter( questions,OwnQuestionActivity.this);
                rvOwnQuestion.setAdapter(questionAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
