package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Model.User;
import com.susthoxyz.sustho.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OtpActivity extends AppCompatActivity {

    private String key = "fuck_you_proloy";
    private String login_key = "again_fuck_you_proloy" ;
    private String payment = "0";
    private String blood = "N/A" ;
    private FirebaseAuth mAuth;
    private String reg_url = "http://api.sustho.xyz/sustho/user_registration.php" ;
    private String loginUrl = "http://api.sustho.xyz/sustho/user_login.php" ;
    private String profileImage = "https://firebasestorage.googleapis.com/v0/b/sustho-3b3e4.appspot.com/o/user_icon.png?alt=media&token=158851d5-c9ad-47df-9ca8-2402171a3a99";
    private String verificationid ,otp,type,id;
    private String name = null ;
    private String mobile = null ;
    private String gender = null ;
    private Pinview pvRegistrationSecurity ;
    private DatabaseReference reference;
    private Button btnNext ;
    private SharedPreferences sharedPreferences ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        reference = FirebaseDatabase.getInstance().getReference().child("Users");
        sharedPreferences = (OtpActivity.this).getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name",null);
        mobile = sharedPreferences.getString("mobile",null);
        gender = sharedPreferences.getString("gender",null);

        Toast.makeText(this, "কোড এর জন্য অপেক্ষা করুন", Toast.LENGTH_SHORT).show();

        pvRegistrationSecurity = findViewById(R.id.pvSecurity);
        btnNext = findViewById(R.id.btnCodeNext);
        mAuth = FirebaseAuth.getInstance();

        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        sendVerificationCode(mobile);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                otp = pvRegistrationSecurity.getValue();

                if ((otp.isEmpty() || otp.length() < 6)){

                    Toast.makeText(getApplicationContext(),"কোডটি ঠিকমত দিন",Toast.LENGTH_SHORT).show();
                }

                else {
                    verifyCode(otp);
                }

            }
        });


    }


    private void sendVerificationCode(String mobile) {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationid = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null){
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(),Toast.LENGTH_LONG).show();

        }
    };

    private void verifyCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationid, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)

                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            if(type.equals("register"))
                            {

                                String key = reference.push().getKey();

                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("tokenKey",mobile);
                                editor.commit();

                                User user = new User(mobile,name,gender,blood,profileImage,payment,key);

                                reference.child(key).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        Toast.makeText(OtpActivity.this,"সফলভাবে রেজিস্টার হয়েছে",Toast.LENGTH_SHORT).show();

                                        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                            }

                            else {
                                Toast.makeText(OtpActivity.this, "সফলভাবে লগ ইন হয়েছে", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }

                });
    }

    /*private void register() {

        StringRequest request = new StringRequest(Request.Method.POST, reg_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String token = jsonObject.getString("token");
                            id = jsonObject.getString("user_id");

                            if (status.equals("1"))
                            {

                                Toast.makeText(OtpActivity.this,"সফলভাবে রেজিস্টার হয়েছে",Toast.LENGTH_SHORT).show();
                                sharedPreferences = (OtpActivity.this).getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("token",token);
                                editor.putString("mobile",mobile);
                                editor.putString("user_id",id);
                                editor.commit();

                                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                Toast.makeText(OtpActivity.this,status,Toast.LENGTH_SHORT).show();

                            }

                        }
                        catch (Exception e){
                            e.printStackTrace();
                            Toast.makeText(OtpActivity.this,"Error" + e.toString(),Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OtpActivity.this,"Register Error" + error.toString(),Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> params = new HashMap<>();
                params.put("username",name);
                params.put("phn_no",mobile);
                params.put("blood_group",blood);
                params.put("gender",gender);
                params.put("registration",key);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }*/

    /*private void login() {

        StringRequest request = new StringRequest(Request.Method.POST, loginUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String token = jsonObject.getString("token");

                            if (status.equals("1")) {

                                JSONArray dataArray = jsonObject.getJSONArray("user_login");

                                for (int i = 0; i < dataArray.length(); i++) {

                                    JSONObject dataobj = dataArray.getJSONObject(i);

                                    Toast.makeText(OtpActivity.this, "সফলভাবে লগ ইন হয়েছে" + status, Toast.LENGTH_SHORT).show();

                                    id = dataobj.getString("user_id");

                                    sharedPreferences = (OtpActivity.this).getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("token", token);
                                    editor.putString("mobile", mobile);
                                    editor.putString("user_id", id);
                                    editor.commit();

                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                            else {
                                Toast.makeText(OtpActivity.this,status,Toast.LENGTH_SHORT).show();

                            }

                        }
                        catch (Exception e){
                            e.printStackTrace();
                            Toast.makeText(OtpActivity.this,"Error" + e.toString(),Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OtpActivity.this,"Register Error" + error.toString(),Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> params = new HashMap<>();
                params.put("phn_no",mobile);
                params.put("login",login_key);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }*/

}
