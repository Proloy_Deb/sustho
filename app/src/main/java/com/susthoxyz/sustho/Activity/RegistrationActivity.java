package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.FirebaseDatabase;
import com.susthoxyz.sustho.Model.User;
import com.susthoxyz.sustho.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RegistrationActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    private Button btnRegistration ;
    private String gender,name,mobile ;
    private RadioGroup rgGender;
    private TextView  tvLogin ;
    private EditText etRegistrationName,etRegistrationMobile ;
    private SharedPreferences sharedPreferences ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        btnRegistration = findViewById(R.id.btnRegistration);
        etRegistrationName = findViewById(R.id.etRegistrationName);
        etRegistrationMobile = findViewById(R.id.etRegistrationMobile);
        btnRegistration = findViewById(R.id.btnRegistration);
        tvLogin=findViewById(R.id.tvLogin);
        rgGender=findViewById(R.id.rgGender);

        rgGender.setOnCheckedChangeListener(this);


        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = etRegistrationName.getText().toString();
                mobile = "+88"+etRegistrationMobile.getText().toString();


                sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("mobile",mobile);
                editor.putString("name",name);
                editor.putString("gender",gender);
                editor.commit();

                if((name.length()==0)||(mobile.length()==0))
                {
                    Toast.makeText(RegistrationActivity.this, "নাম - মোবাইল ফিল আপ করুন", Toast.LENGTH_SHORT).show();
                }

                else {
                    Intent intent = new Intent(getApplicationContext(),OtpActivity.class);
                    intent.putExtra("type","register");
                    startActivity(intent);
                }

            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId){
            case R.id.rvMale:
                gender = "ছেলে";
                break;

            case R.id.rvFeMale:
                gender = "মেয়ে";
                break;
        }
    }
}
