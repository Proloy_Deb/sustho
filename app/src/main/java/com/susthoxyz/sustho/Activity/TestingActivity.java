package com.susthoxyz.sustho.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.susthoxyz.sustho.Model.Area;
import com.susthoxyz.sustho.Model.OnlineDoctor;
import com.susthoxyz.sustho.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestingActivity extends AppCompatActivity {

    Spinner sp ;
    String url = "http://api.sustho.xyz/sustho/get_area.php";
    ArrayList<String> area;

    String user_id = "6";
    private String token = "7af626edb31c54fa9245fc1c00809bf6dba5bcb9";
    private String key = "give_all_area";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        area = new ArrayList<>();
        sp = findViewById(R.id.spinner);
        loadSpinnerData(url);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String area =  sp.getItemAtPosition(sp.getSelectedItemPosition()).toString();
                Toast.makeText(getApplicationContext(),area,Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

    }

    private void loadSpinnerData(String url) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray dataArray = jsonObject.getJSONArray("area");

                            for (int i = 0; i < dataArray.length(); i++) {

                                JSONObject dataobj = dataArray.getJSONObject(i);

                                String area_name = dataobj.getString("area_name");
                                area.add(area_name);
                            }
                            sp.setAdapter(new ArrayAdapter<String>(TestingActivity.this, android.R.layout.simple_spinner_dropdown_item, area));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("token",token);
                params.put("area",key);

                return params;
            }
        };

        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }
}
