package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Fragment.PaymentFragment;
import com.susthoxyz.sustho.OnlineQuestion.Answer;
import com.susthoxyz.sustho.OnlineQuestion.QuestionDetailAdapter;
import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;

public class OnlineChatActivity extends AppCompatActivity {

    RecyclerView recyclerView ;
    QuestionDetailAdapter questionAdapter ;
    ArrayList<Question> questions;
    private EditText etQuestion ;
    private ImageView imvQuestionSend ;
    private String writeQuestion,department,payment ;
    private SharedPreferences sharedPreferences ;
    DatabaseReference reference,userreference;
    String parentKey = null;
    String type = "chatquestion";
    RelativeLayout rlChat ;
    private String name,mobile ;
    private Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_chat);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        mobile = sharedPreferences.getString("mobile",null);
        name = sharedPreferences.getString("name",null);
        department = sharedPreferences.getString("onlinedepartment",null);

        etQuestion = findViewById(R.id.etOnlineChattingQuestion);
        toolbar = findViewById(R.id.toolBarChat);
        rlChat = findViewById(R.id.rlSendMessage);
        imvQuestionSend = findViewById(R.id.imvSend);
        recyclerView = findViewById(R.id.rvOnlineChat);
        rlChat.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        reference = FirebaseDatabase.getInstance().getReference().child("onlincedoctor");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        seeChats();
        cheack();
        imvQuestionSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postQuestion();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void cheack() {
        userreference = FirebaseDatabase.getInstance().getReference().child("Users");

        userreference.child(mobile).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                payment = dataSnapshot.child("payment").getValue().toString();

                if (payment.equals("1"))
                {
                    rlChat.setVisibility(View.VISIBLE);
                }

                else {
                    PaymentFragment paymentFragment = new PaymentFragment();
                    paymentFragment.show(getSupportFragmentManager(),"payment");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void postQuestion() {

        writeQuestion = etQuestion.getText().toString();
        String key = reference.push().getKey();
        Answer question = new Answer(writeQuestion,mobile,name,key,type,parentKey);

        reference.child(department).child(mobile).child(key).setValue(question).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "Post Successfully", Toast.LENGTH_SHORT).show();
                etQuestion.getText().clear();
            }
        });
    }

    private void seeChats() {

        reference.child(department).child(mobile).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                questions = new ArrayList<Question>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                        Question question = dataSnapshot1.getValue(Question.class);
                        questions.add(question);

                }
                questionAdapter = new QuestionDetailAdapter( questions,getApplicationContext());
                recyclerView.setAdapter(questionAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
