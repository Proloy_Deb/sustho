package com.susthoxyz.sustho.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.susthoxyz.sustho.R;

public class SplashActivity extends AppCompatActivity {

    private ImageView imvSlapshLogo,imvDesign ;
    private static int splashTime = 3000;
    private SharedPreferences sharedPreferences ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        imvSlapshLogo = findViewById(R.id.imvSplashLogo);
        imvDesign = findViewById(R.id.imvLogoDesign);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                /*String checkId = null ;
                sharedPreferences = (SplashActivity.this).getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                checkId = sharedPreferences.getString("token",null);

                if(checkId != null)
                {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    SplashActivity.this.finish();
                }
                else {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, RegistrationActivity.class));
                    SplashActivity.this.finish();
                }*/

                SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                SplashActivity.this.finish();

            }
        },splashTime);

        Animation animation = AnimationUtils.loadAnimation(this,R.anim.splashanim);
        imvSlapshLogo.startAnimation(animation);
        Animation animationDesgin = AnimationUtils.loadAnimation(this,R.anim.top);
        imvDesign.startAnimation(animationDesgin);

    }
}
