package com.susthoxyz.sustho.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Adapter.ChatDoctorAdapter;
import com.susthoxyz.sustho.Model.OnlineChat;
import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.OnlineQuestion.QuestionAdapter;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;

public class OnlineDoctorActivity extends AppCompatActivity {

    private RecyclerView rvDoctor ;
    private DatabaseReference databaseReference ;
    ChatDoctorAdapter chatDoctorAdapter ;
    ArrayList<OnlineChat> chats;
    private Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_doctor);

        rvDoctor = findViewById(R.id.rvOnlineDoctor);
        toolbar = findViewById(R.id.toolBarOnline);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("onlinedepartment");
        rvDoctor.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                chats = new ArrayList<OnlineChat>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    OnlineChat onlineChat = dataSnapshot1.getValue(OnlineChat.class);
                    chats.add(onlineChat);

                }
                chatDoctorAdapter = new ChatDoctorAdapter( chats,OnlineDoctorActivity.this);
                rvDoctor.setAdapter(chatDoctorAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
