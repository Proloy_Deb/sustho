package com.susthoxyz.sustho.OnlineQuestion;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.R;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.MyViewHolder> {

    private Context mContext;
    private List<Question> mData;
    SharedPreferences sharedPreferences ;

    public QuestionAdapter(List<Question> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_question, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuestionAdapter.MyViewHolder holder, final int position) {

    try {
        holder.tvQuestion.setText(mData.get(position).getQuestion());
        holder.tvCategory.setText(mData.get(position).getCategory());
        holder.tvDate.setText(mData.get(position).getDate());
        holder.tvTime.setText(mData.get(position).getTime());
    }

    catch (Exception e)
    {

    }

        holder.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String parentKey = mData.get(position).getParentkey() ;
                String question = mData.get(position).getQuestion();
                String mobile = mData.get(position).getMobile();

                sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("parentKey", parentKey);
                editor.putString("question",question);
                editor.putString("questionmobile",mobile);
                editor.commit();

                Intent myactivity = new Intent(mContext.getApplicationContext(), AnswerActivity.class);
                myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.getApplicationContext().startActivity(myactivity);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvQuestion,tvCategory,tvDate,tvTime ;
        Button btnSubmit ;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvQuestion = itemView.findViewById(R.id.tvQuestion);
            btnSubmit = itemView.findViewById(R.id.btnSeeAnswer);
            tvCategory = itemView.findViewById(R.id.tvQuestionCategory);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTime = itemView.findViewById(R.id.tvTime);

        }
    }
}