package com.susthoxyz.sustho.OnlineQuestion;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;
import java.util.Collections;


public class SexFragment extends Fragment {

    private RecyclerView recyclerView ;
    private QuestionAdapter questionAdapter ;
    private ArrayList<Question> questions;
    private DatabaseReference reference;
    private SharedPreferences sharedPreferences ;
    String regMobile = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sex, container, false);

        sharedPreferences = getActivity().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        regMobile = sharedPreferences.getString("mobile",null);
        reference = FirebaseDatabase.getInstance().getReference().child("QuestionAnswer");
        recyclerView = rootView.findViewById(R.id.rvSex);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));;

        seeQuestions();

        return rootView ;
    }

    private void seeQuestions()
    {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                questions = new ArrayList<Question>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    for (DataSnapshot dataSnapshot12 : dataSnapshot1.getChildren()) {

                        for (DataSnapshot dataSnapshot123 : dataSnapshot12.getChildren()) {

                            if((dataSnapshot123.child("type").getValue().toString()).equals("question")) {

                                try {

                                    if((dataSnapshot123.child("categoryid").getValue().toString()).equals("sex"))
                                    {
                                        Question question = dataSnapshot123.getValue(Question.class);
                                        questions.add(question);
                                    }
                                }

                                catch (Exception e)
                                {

                                }
                            }
                        }
                    }
                }

                Collections.reverse(questions);
                questionAdapter = new QuestionAdapter( questions,getActivity());
                recyclerView.setAdapter(questionAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}