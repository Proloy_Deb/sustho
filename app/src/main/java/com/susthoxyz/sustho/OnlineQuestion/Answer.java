package com.susthoxyz.sustho.OnlineQuestion;

public class Answer {

    String question , mobile , name , key, type , parentkey;

    public Answer() {
    }

    public Answer(String question, String mobile, String name, String key, String type, String parentkey) {
        this.question = question;
        this.mobile = mobile;
        this.name = name;
        this.key = key;
        this.type = type;
        this.parentkey = parentkey;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParentkey() {
        return parentkey;
    }

    public void setParentkey(String parentkey) {
        this.parentkey = parentkey;
    }
}
