package com.susthoxyz.sustho.OnlineQuestion;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.R;

import java.util.List;

public class QuestionDetailAdapter extends RecyclerView.Adapter<QuestionDetailAdapter.MyViewHolder> {

    private Context mContext;
    private List<Question> mData;
    private String questionMobile ;
    private SharedPreferences sharedPreferences ;

    public QuestionDetailAdapter(List<Question> mData, Context mContext) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_question_detail, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuestionDetailAdapter.MyViewHolder holder, final int position) {
        holder.tvQuestion.setText(mData.get(position).getQuestion());
        sharedPreferences = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        questionMobile = sharedPreferences.getString("mobile",null);
        String type = mData.get(position).getType();

        if (type.equals("answer")) {
            holder.cvQuestion.setCardBackgroundColor(mContext.getResources().getColor(R.color.headertext));
            holder.tvQuestion.setTextColor(mContext.getResources().getColor(R.color.white));
        }

        if (type.equals("question")) {
            holder.cvQuestion.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvQuestion ;
        RelativeLayout rlQuestionAnswer;
        Button btnSubmit ;
        CardView cvQuestion ;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvQuestion = itemView.findViewById(R.id.tvQuestionDetail);
            cvQuestion = itemView.findViewById(R.id.cvQuestionDetail);

        }
    }
}