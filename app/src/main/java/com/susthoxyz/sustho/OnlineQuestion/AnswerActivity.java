package com.susthoxyz.sustho.OnlineQuestion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.R;

import java.util.ArrayList;

public class AnswerActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences ;
    private String parentKey , questionMobileNumber,reply ,name;
    private String mobile = null;
    private EditText etQuestion ;
    RecyclerView recyclerView ;
    QuestionDetailAdapter questionDetailAdapter ;
    private ImageView imvQuestionAnswer ;
    ArrayList<Question> questions;
    DatabaseReference reference;
    RelativeLayout rlAnswer ;
    String type = "reply";
    private Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name",null);
        mobile = sharedPreferences.getString("mobile",null);

        recyclerView = findViewById(R.id.rvAnswerDetail);
        rlAnswer = findViewById(R.id.rlQuestionAnswer);
        imvQuestionAnswer = findViewById(R.id.imvQuestionAnswer);
        etQuestion = findViewById(R.id.etQuestionAnswer);
        imvQuestionAnswer = findViewById(R.id.imvQuestionAnswer);
        toolbar = findViewById(R.id.toolBarDetail);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        sharedPreferences = getApplicationContext().getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        parentKey = sharedPreferences.getString("parentKey",null);
        questionMobileNumber = sharedPreferences.getString("questionmobile",null);


        rlAnswer.setVisibility(View.GONE);

        if (mobile != null)
        {
            if (!mobile.equals(questionMobileNumber))
            {
                rlAnswer.setVisibility(View.GONE);
            }

            if (mobile.equals(questionMobileNumber))
            {
                rlAnswer.setVisibility(View.VISIBLE);
            }
        }

        reference = FirebaseDatabase.getInstance().getReference().child("QuestionAnswer").child(questionMobileNumber);

        reference.child(parentKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                questions = new ArrayList<Question>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Question question = dataSnapshot1.getValue(Question.class);
                    questions.add(question);
                }

                questionDetailAdapter = new QuestionDetailAdapter( questions,AnswerActivity.this);
                recyclerView.setAdapter(questionDetailAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });

        imvQuestionAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postQuestion();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void postQuestion() {

        reply = etQuestion.getText().toString();
        String key = reference.push().getKey();
        Answer question = new Answer(reply,mobile,name,key,type,parentKey);

        reference.child(parentKey).child(key).setValue(question).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "Post Successfully", Toast.LENGTH_SHORT).show();
                etQuestion.getText().clear();
            }
        });
    }
}
