package com.susthoxyz.sustho.OnlineQuestion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.susthoxyz.sustho.Activity.LoginActivity;
import com.susthoxyz.sustho.Activity.RegistrationActivity;
import com.susthoxyz.sustho.Model.Question;
import com.susthoxyz.sustho.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class QuestionAnswer extends AppCompatActivity {

    private DatabaseReference reference;
    private EditText etWriteQuestion ;
    private Button btnPostQuestion ;
    private Toolbar toolbar ;
    private SharedPreferences sharedPreferences ;
    private String writeQuestion;
    private String category = "all";
    private String regMobile ;
    private String type = "question";
    private String name = null;
    private TabLayout tabLayout;
    private ViewPager viewPager ;
    private String token = null;

//    APIService apiService ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_answer);

        sharedPreferences = getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
        regMobile = sharedPreferences.getString("mobile",null);
        name = sharedPreferences.getString("name",null);
        token = sharedPreferences.getString("token",null);
        reference = FirebaseDatabase.getInstance().getReference().child("QuestionAnswer");

 //       apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        etWriteQuestion = findViewById(R.id.etQuestion);
        btnPostQuestion = findViewById(R.id.btnSubmit);
        toolbar = findViewById(R.id.toolBarQuestion);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabQuestion);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(new AllFragment()," সব ধরণের প্রশ্ন");
        viewPagerAdapter.addFragment(new NoseFragment()," নাক,কান ও গলা ");
        viewPagerAdapter.addFragment(new PainFragment()," বাত ও ব্যাথা ");
        viewPagerAdapter.addFragment(new SkinFragment(),"  চর্ম ও অ্যালার্জি");
        viewPagerAdapter.addFragment(new UrologyFragment()," ইউরোলজি ");
        viewPagerAdapter.addFragment(new SexFragment()," প্রথমিক যৌন জ্ঞান");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                questions = new ArrayList<Question>();

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    for (DataSnapshot dataSnapshot12 : dataSnapshot1.getChildren()) {

                        for (DataSnapshot dataSnapshot123 : dataSnapshot12.getChildren()) {

                            if((dataSnapshot123.child("type").getValue().toString()).equals("question")) {

                                Question question = dataSnapshot123.getValue(Question.class);
                                questions.add(question);
                            }
                        }
                    }
                }
                Collections.reverse(questions);
                questionAdapter = new QuestionAdapter( questions,QuestionAnswer.this);
                recyclerView.setAdapter(questionAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Opsss.... Something is wrong", Toast.LENGTH_SHORT).show();
            }
        });*/

        btnPostQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(token != null)
                {
                    postQuestion();
                }

                else {
                    Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                    startActivity(intent);
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void postQuestion() {

        writeQuestion = etWriteQuestion.getText().toString();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        String date = dateFormat.format(calendar.getTime());
        String time = timeFormat.format(calendar.getTime());

        if(writeQuestion.length()==0)
        {
            etWriteQuestion.setError("প্রশ্ন লিখুন");
            Toast.makeText(this, date+time, Toast.LENGTH_SHORT).show();
        }

        else {

            String parentKey = reference.push().getKey();
            String key = reference.push().getKey();
            Question question = new Question(writeQuestion,regMobile,name,key,type,parentKey,category,date,time);

            reference.child(regMobile).child(parentKey).child(key).setValue(question).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
//                    sendNotification(rec);
                    Toast.makeText(getApplicationContext(), "Post Successfully", Toast.LENGTH_SHORT).show();
                    etWriteQuestion.getText().clear();
                }
            });
        }

    }

    private void sendNotification(String reciever,String username,String message)
    {

    }

}
